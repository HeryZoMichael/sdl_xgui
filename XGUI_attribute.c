/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_attribute.h"

void XGUI_test_attr_and_write_value( const char * attr_name, char * value_str , XGUI_Element *element , XGUI_Element *search_chained_list)
{
    /* A VALEUR DANS UN FICHIER Format : {{nom_fichier}} */
    if( value_str != NULL && strlen( value_str ) >= 5 && value_str[0] == '{' && value_str[1] == '{' && value_str[strlen(value_str)-2] == '}' && value_str[strlen(value_str)-1] == '}' )
    {
        XGUI_val_from_file( value_str );
    }
    /* A VALEUR DANS UN FICHIER */


	/*strlen(9999)=4*/


    /*X*/
    if(strcmp(attr_name, "x") == 0 && strlen(value_str) <= 10 )
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->x = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            element->x = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;

        }
    }
    /*X*/


    /*Y*/
    if(strcmp(attr_name, "y") == 0 && strlen(value_str) <= 10 )
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->y = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            element->y = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
        }
    }
    /*Y*/


    /*WIDTH*/
    else if( (strcmp(attr_name, "width") == 0 || strcmp(attr_name, "w") == 0) && strlen(value_str) <= 10 )
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->width = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->width = (int)( strtod(value_str, NULL) * (double)element->parent->width / 100 );

                element->width = element->width >= 0 ? element->width : 0;
            }
            else
            {
                element->width = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        if( element->min_width_set )
        {
            if( element->width < element->min_width )
            {
                element->width = element->min_width;
            }
        }

        if( element->max_width_set )
        {
            if( element->width > element->max_width )
            {
                element->width = element->max_width;
            }
        }

        element->width_set = true;
    }
    /*WIDTH*/


    /*HEIGTH*/
    else if( (strcmp(attr_name, "height") == 0 || strcmp(attr_name, "h") == 0) && strlen(value_str) <= 10 )
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->height = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->height = (int)( strtod(value_str, NULL) * (double)element->parent->height / 100 );

                element->height = element->height >= 0 ? element->height : 0;
            }
            else
            {
                element->height = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        if( element->min_height_set )
        {
            if( element->height < element->min_height )
            {
                element->height = element->min_height;
            }
        }

        if( element->max_height_set )
        {
            if( element->height > element->max_height )
            {
                element->height = element->max_height;
            }
        }

        element->height_set = true;
    }
    /*HEIGTH*/


    /*MAX_WIDTH*/
    else if((strcmp(attr_name, "max-width") == 0 || strcmp(attr_name, "max-w") == 0) && strlen(value_str) <= 10)
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->max_width = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->max_width = (int)( strtod(value_str, NULL) * (double)element->parent->max_width / 100 );

                element->max_width = element->max_width >= 0 ? element->max_width : 0;
            }
            else
            {
                element->max_width = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        if( element->width_set )
        {
            if( element->width > element->max_width )
            {
                element->width = element->max_width;
            }
        }

        element->max_width_set = true;
    }
    /*MAX_WIDTH*/


    /*MIN_WIDTH*/
    else if((strcmp(attr_name, "min-width") == 0 || strcmp(attr_name, "min-w") == 0) && strlen(value_str) <= 10)
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->min_width = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->min_width = (int)( strtod(value_str, NULL) * (double)element->parent->min_width / 100 );

                element->min_width = element->min_width >= 0 ? element->min_width : 0;
            }
            else
            {
                element->min_width = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        if( element->width_set )
        {
            if( element->width < element->min_width )
            {
                element->width = element->min_width;
            }
        }

        element->min_width_set = true;
    }
    /*MIN_WIDTH*/


    /*MAX_HEIGHT*/
    else if((strcmp(attr_name, "max-height") == 0 || strcmp(attr_name, "max-h") == 0) && strlen(value_str) <= 10)
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->max_height = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->max_height = (int)( strtod(value_str, NULL) * (double)element->parent->max_height / 100 );

                element->max_height = element->max_height >= 0 ? element->max_height : 0;
            }
            else
            {
                element->max_height = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        if( element->height_set )
        {
            if( element->height > element->max_height )
            {
                element->height = element->max_height;
            }
        }

        element->max_height_set = true;
    }
    /*MAX_HEIGHT*/


    /*MIN_HEIGHT*/
    else if((strcmp(attr_name, "min-height") == 0 || strcmp(attr_name, "min-h") == 0) && strlen(value_str) <= 10)
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->min_height = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->min_height = (int)( strtod(value_str, NULL) * (double)element->parent->min_height / 100 );

                element->min_height = element->min_height >= 0 ? element->min_height : 0;
            }
            else
            {
                element->min_height = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        if( element->height_set )
        {
            if( element->height < element->min_height )
            {
                element->height = element->min_height;
            }
        }

        element->min_height_set = true;
    }
    /*MIN_HEIGHT*/


    /* OPACITY */
    else if(strcmp(attr_name, "opacity") == 0 && strlen(value_str) <= 4 )
    {
        element->opacity = (uint8_t)(atoi( value_str ) >= 0 ? atoi( value_str ) : 0);
    }
    /* OPACITY */


    /*VISIBILITY*/
    else if(strcmp(attr_name, "visibility") == 0)
    {
        if( strcmp(value_str, "1") || strcmp(value_str, "visible") == 0 )
        {
            element->visibility = true;
        }
        else if( strcmp(value_str, "0") || strcmp(value_str, "hidden") == 0 )
        {
            element->visibility = false;
        }
    }
    /*VISIBILITY*/


    /*DECAL_LEFT*/
    else if(strcmp(attr_name, "decal-left") == 0 && strlen(value_str) <= 10)
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->decal_left = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->decal_left = (int)( strtod(value_str, NULL) * (double)element->parent->width / 100 );
            }
            else
            {
                element->decal_left = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        element->decal_left_set = true;

        if( element->position == XGUI_POSITION_ABSOLUTE && element->decal_right_set && element->decal_left_set)
        {
            element->width_set = true;
        }
    }
    /*DECAL_LEFT*/


	/*DECAL_TOP*/
	else if(strcmp(attr_name, "decal-top") == 0 && strlen(value_str) <= 10)
	{
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->decal_top = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->decal_top = (int)( strtod(value_str, NULL) * (double)element->parent->height / 100 );
            }
            else
            {
                element->decal_top = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        element->decal_top_set = true;

        if( element->position == XGUI_POSITION_ABSOLUTE && element->decal_top_set && element->decal_bottom_set )
        {
            element->height_set = true;
        }
	}
	/*DECAL_TOP*/

	/*DECAL_RIGHT*/
	else if(strcmp(attr_name, "decal-right") == 0 && strlen(value_str) <= 10)
	{
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->decal_right = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->decal_right = (int)( strtod(value_str, NULL) * (double)element->parent->width / 100 );
            }
            else
            {
                element->decal_right = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        element->decal_right_set = true;

        if( element->position == XGUI_POSITION_ABSOLUTE && element->decal_right_set && element->decal_left_set )
        {
            element->width_set = true;
        }
	}
    /*DECAL_RIGHT*/


    /*DECAL_BOTTOM*/
    else if(strcmp(attr_name, "decal-bottom") == 0 && strlen(value_str) <= 10)
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->decal_bottom = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            if( value_str[strlen(value_str)-1] == '%' )
            {
                element->decal_bottom = (int)( strtod(value_str, NULL) * (double)element->parent->height / 100 );
            }
            else
            {
                element->decal_bottom = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
            }
        }

        element->decal_bottom_set = true;

        if( element->position == XGUI_POSITION_ABSOLUTE && element->decal_top_set && element->decal_bottom_set )
        {
            element->height_set = true;
        }
    }
    /*DECAL_BOTTOM*/


    /*CURSOR*/
    else if(strcmp(attr_name, "cursor") == 0 || strcmp(attr_name, "cur") == 0)
    {
        if( strcmp(value_str, "load") == 0 )
        {
            element->cursor=LOAD;
        }
        else if( strcmp(value_str, "pointer") == 0 )
        {
            element->cursor=HAND;
        }
        else if( strcmp(value_str, "search") == 0 )
        {
            element->cursor=SEARCH;
        }
        else if( strcmp(value_str, "arrow") == 0 )
        {
            element->cursor=ARROW;
        }
        else if( strcmp(value_str, "ibeam") == 0 )
        {
            element->cursor=IBEAM;
        }
        else
        {
            element->cursor=DEFAULT;
        }
    }
    /*CURSOR*/


    /*ALIGNEMENT*/
    else if(strcmp(attr_name, "alignement") == 0)
    {
        if( strcmp(value_str, "l") == 0 || strcmp(value_str, "left") == 0 )
        {
            element->alignement=LEFT;
        }
        else if( strcmp(value_str, "r") == 0 || strcmp(value_str, "right") == 0 )
        {
            element->alignement=RIGHT;
        }
        else if( strcmp(value_str, "c") == 0 || strcmp(value_str, "center") == 0 )
        {
            element->alignement=CENTER;
        }
    }
    /*ALIGNEMENT*/


    /*TYPE*/
    else if( strcmp(attr_name, "type") == 0 )
    {
        if( strcmp(value_str, "bloc") == 0 )
        {
            element->type = XGUI_TYPE_BLOC;
        }
        else if( strcmp(value_str, "image") == 0 )
        {
            element->type = XGUI_TYPE_IMAGE;
        }
        else if( strcmp(value_str, "text") == 0 )
        {
            element->type = XGUI_TYPE_TEXT;
        }
        else if( strcmp(value_str, "password") == 0 )
        {
            element->type = XGUI_TYPE_PASSWORD;
        }
        else if( strcmp(value_str, "url") )
        {
            element->type = XGUI_TYPE_URL;
        }
        else if( strcmp(value_str, "item") == 0 || strcmp(value_str, "item-0") == 0 )
        {
            element->type = XGUI_TYPE_ITEM_0;
        }
        else if( strcmp(value_str, "item-1") == 0 )
        {
            element->type = XGUI_TYPE_ITEM_1;
        }
        else if( strcmp(value_str, "item-2") == 0 )
        {
            element->type = XGUI_TYPE_ITEM_2;
        }
        else if( strcmp(value_str, "item-3") == 0 )
        {
            element->type = XGUI_TYPE_ITEM_3;
        }
        else if( strcmp(value_str, "item-4") == 0 )
        {
            element->type = XGUI_TYPE_ITEM_4;
        }
        else
        {
            element->type = XGUI_TYPE_DEFAULT;
        }
    }
    /*TYPE*/


    /*ID*/
    else if( strcmp(attr_name, "id") == 0 )
    {
        sprintf( element->id, "%s", value_str );
    }
    /*ID*/


    /*BACKGROUND_COLOR*/
    else if( (strcmp(attr_name, "background") == 0 || strcmp(attr_name, "background-color") == 0) && strlen(value_str) <= 20 )
    {
        element->background_color = XGUI_get_color_from_code( value_str );
    }
    /*BACKGROUND_COLOR*/


    /*COLOR*/
    else if( (strcmp(attr_name, "color") == 0 || strcmp(attr_name, "text-color") == 0 ) && strlen(value_str) <= 20 )
    {
        element->text_color = XGUI_get_color_from_code( value_str );
    }
    /*COLOR*/


	/* IMAGE SOURCE || TXT VALUE*/
	else if( strcmp(attr_name, "value") == 0 || strcmp(attr_name, "src") == 0 || strcmp(attr_name, "_") == 0 )
	{
        XGUI_secure_write( value_str , element->type_data_source );
	}
	/* IMAGE SOURCE || TXT VALUE*/


	/*BACKGROUND_IMAGE_SOURCE*/
	else if( strcmp(attr_name, "background-image") == 0 )
	{
        XGUI_secure_write( value_str , element->background_image_source );
	}
	/*BACKGROUND_IMAGE_SOURCE*/


    /*BORDER_TYPE*/
    else if(strcmp(attr_name, "border-type") == 0)
    {
        if(	strcmp(value_str, "radius") == 0 )
        {
            element->border_type = XGUI_BORDER_TYPE_RADIUS;
        }
        else if( strcmp(value_str, "chanfrein") == 0 )
        {
            element->border_type = XGUI_BORDER_TYPE_CHANFREIN;
        }
        else
        {
            element->border_type = XGUI_BORDER_TYPE_NONE;
        }
    }
    /*BORDER_TYPE*/


    /*POSITION*/
    else if(strcmp(attr_name, "position") == 0)
    {
        if(strcmp(value_str, "absolute") == 0)
        {
            element->position = XGUI_POSITION_ABSOLUTE;

            if( element->decal_right_set && element->decal_left_set )
            {
                element->width_set = true;
            }

            if( element->decal_top_set && element->decal_bottom_set )
            {
                element->height_set = true;
            }
        }
        else
        {
            element->position = XGUI_POSITION_NORMAL;
        }
    }
    /*POSITION*/


    /*FONT_SIZE*/
    else if(strcmp(attr_name, "font-size") == 0 && strlen(value_str) <= 5 )
    {
        element->font_size = (uint8_t)(atoi( value_str ) >= 0 ? atoi( value_str ) : 0);
    }
    /*FONT_SIZE*/

    /*FONT*/
    else if(strcmp(attr_name, "font") == 0)
    {
        sprintf( element->police_name, "%s", value_str );
    }
    /*FONT*/


    /*REFERENCE*/
    else if( strcmp(attr_name, "reference") == 0 || strcmp(attr_name, "ref") == 0 )
    {
        sprintf( element->reference_id, "%s", value_str );
    }
    /*REFERENCE*/


	/*BORDER_VALUE*/
	else if( ( strcmp(attr_name, "border-radius") == 0 || strcmp(attr_name, "border-chanfrein") == 0 ) && strlen(value_str) <= 20 )
	{
        double d_border_tl, d_border_tr, d_border_br, d_border_bl;

		if( sscanf(value_str, "%lf %lf %lf %lf", &d_border_tl, &d_border_tr, &d_border_br, &d_border_bl) == 4 )
		{
            element->border_values = (XGUI_Border_Value_Struct){ d_border_tl, d_border_tr, d_border_br, d_border_bl };

            if( !(d_border_tl == 0 && d_border_tr == 0 && d_border_br == 0 && d_border_bl == 0) )
			{
				element->cumul_border_values = element->parent->cumul_border_values + 1;
			}
		}

		if( strcmp(attr_name, "border-radius") == 0 )
		{
			element->border_type = XGUI_BORDER_TYPE_RADIUS;
		}
		else if( strcmp(attr_name, "border-chanfrein") == 0 )
		{
			element->border_type = XGUI_BORDER_TYPE_CHANFREIN;
		}
	}
	/*BORDER_VALUE*/


    /*FONT_STYLE*/
    else if( strcmp(attr_name, "font-style") == 0 )
    {
        int tableau_font_style[4];

        tableau_font_style[0] = 0; /* NORMAL */
        tableau_font_style[1] = 0; /* BOLD */
        tableau_font_style[2] = 0; /* ITALIC */
        tableau_font_style[3] = 0; /* UNDERLINE */

        if( memchr( value_str, 'n', strlen(value_str) ) != NULL )
        {
            tableau_font_style[0] = 1;
        }
        else
        {
            if( memchr( value_str, 'b', strlen(value_str) ) != NULL )
            {
                tableau_font_style[1] = 1;
            }
            if( memchr( value_str, 'i', strlen(value_str) ) != NULL )
            {
                tableau_font_style[2] = 1;
            }
            if( memchr( value_str, 'u', strlen(value_str) ) != NULL )
            {
                tableau_font_style[3] = 1;
            }

            if( tableau_font_style[1] && !tableau_font_style[2] && !tableau_font_style[3] )
            {
                element->font_style = 1; /* XGUI_FONT_STYLE_BOLD */
            }
            else if( !tableau_font_style[1] && tableau_font_style[2] && !tableau_font_style[3] )
            {
                element->font_style = 2; /* XGUI_FONT_STYLE_ITALIC */
            }
            else if( tableau_font_style[1] && tableau_font_style[2] && !tableau_font_style[3] )
            {
                element->font_style = 3; /* XGUI_FONT_STYLE_UNDERLINED */
            }
            else if( !tableau_font_style[1] && !tableau_font_style[2] && tableau_font_style[3] )
            {
                element->font_style = 4; /* XGUI_FONT_STYLE_BOLD_ITALIC */
            }
            else if( tableau_font_style[1] && !tableau_font_style[2] && tableau_font_style[3] )
            {
                element->font_style = 5; /* XGUI_FONT_STYLE_BOLD_UNDERLINED*/
            }
            else if( !tableau_font_style[1] && tableau_font_style[2] && tableau_font_style[3] )
            {
                element->font_style = 6; /* XGUI_FONT_STYLE_ITALIC_UNDERLINED */
            }
            else if( tableau_font_style[1] && tableau_font_style[2] && tableau_font_style[3] )
            {
                element->font_style = 7; /* XGUI_FONT_STYLE_BOLD_ITALIC_UNDERLINED */
            }
            else
            {
                element->font_style = 0;
            }
        }
    }
    /*FONT_STYLE*/


    /*SCROLL _ X*/
    else if( strcmp(attr_name, "scroll-x") == 0 && strlen(value_str) <= 10 )
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->scroll_x = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            element->scroll_x = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
        }
    }
    /*SCROLL_X*/


    /*SCROLL_Y*/
    else if( strcmp(attr_name, "scroll-y") == 0 && strlen(value_str) <= 10 )
    {
        if(value_str[0] == ')' && value_str[1] != '\0')
        {
            element->scroll_y = XGUI_val_from_reference( value_str , element->reference_id , search_chained_list);
        }
        else
        {
            element->scroll_y = atoi( value_str ) >= 0 ? atoi( value_str ) : 0;
        }
    }
    /*SCROLL _ Y*/

}


void XGUI_secure_write( char *value_str , char *target )
{
    /* Pour certaines attr, il est possible d'avoir plus de XGUI_MEM_BLOC_LENGTH_MAX caractères
    * Ex : image_source, background_image_source
    */

    char *type_data_source_realloc = NULL;

    if( strlen(value_str) <= XGUI_MEM_BLOC_LENGTH_MAX )
    {
        sprintf( target, "%s", value_str );
    }
    else
    {
        type_data_source_realloc = (char*)malloc( (strlen(value_str)+1) * sizeof(char) );

        if( type_data_source_realloc != NULL )
        {
            if( (type_data_source_realloc = memset( type_data_source_realloc , '\0' , (strlen(value_str)+1)*sizeof(char)) ) )
            {
                free( target );

                target = type_data_source_realloc;

                sprintf( target, "%s", value_str );

                return;
            }

            free( type_data_source_realloc );
        }

        /* NE RIEN FAIRE */

        value_str[XGUI_MEM_BLOC_LENGTH_MAX] = '\0';

        sprintf( target , "%s", value_str );
    }

    return;
}

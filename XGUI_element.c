/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_element.h"


/* Debut creation STRUCT */
XGUI_Element* XGUI_new_element( )
{
    XGUI_Element *new_element = NULL;

    new_element = (XGUI_Element*)malloc( sizeof(XGUI_Element) );

    if( new_element == NULL )
    {
        errno = ENOMEM;

        return NULL;
    }

    /* rule : Do not assume memory allocation routines initialize memory */

    if( new_element != memset( new_element , 0 , sizeof(XGUI_Element)) )
    {
        errno = ENOMEM;

        free( new_element );

        return NULL;
    }

    /* malloc pour les membres et valeur pr défaut */

    new_element->parent = NULL;
    new_element->previous_element = NULL;
    new_element->next_element = NULL;
    new_element->last_not_abs_pos_child = NULL;

    new_element->border_values = (XGUI_Border_Value_Struct){ 0 , 0 , 0 , 0 };

    new_element->texture = NULL;
    new_element->background_texture = NULL;
    new_element->police = NULL;

    new_element->background_color = (SDL_Color){ 255 , 255 , 255 , 255 };
    new_element->text_color = (SDL_Color){ 0 , 0 , 0 , 0 };

    new_element->alignement = LEFT;
    new_element->cursor = DEFAULT;


    /* debut malloc char *id , *reference_id , *police_name , *background_image_source , *type_data_source; */

    new_element->id = NULL;
    new_element->reference_id = NULL;
    new_element->police_name = NULL;
    new_element->background_image_source = NULL;
    new_element->type_data_source = NULL;

    new_element->id = malloc( (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) );
    if( new_element->id == NULL )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    if( new_element->id != memset( new_element->id , '\0' , (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) ) )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    sprintf( new_element->id, "__DFL__" );

    new_element->reference_id = malloc( (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) );
    if( new_element->reference_id == NULL )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    if( new_element->reference_id != memset( new_element->reference_id , '\0' , (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) ) )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    sprintf( new_element->reference_id, "__DFL__" );

    new_element->police_name = malloc( (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) );
    if( new_element->police_name == NULL )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    if( new_element->police_name != memset( new_element->police_name , '\0' , (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) ) )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    sprintf( new_element->police_name, "__DFL__" );

    new_element->background_image_source = malloc( (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) );
    if( new_element->background_image_source == NULL )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    if( new_element->background_image_source != memset( new_element->background_image_source , '\0' , (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) ) )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    sprintf( new_element->background_image_source, "__DFL__" );

    new_element->type_data_source = malloc( (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) );
    if( new_element->type_data_source == NULL )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    if( new_element->type_data_source != memset( new_element->type_data_source , '\0' , (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) ) )
    {
        errno = ENOMEM;
        XGUI_free_element( new_element );
        return NULL;
    }
    sprintf( new_element->type_data_source, "__DFL__" );

    /* Fin malloc char *id , *reference_id , *police_name , *image_source , *background_image_source , *type_data_source; */



    new_element->x = 0;
    new_element->y = 0;
    new_element->width = 0;
    new_element->height = 0;
    new_element->max_width = 0;
    new_element->max_height = 0;
    new_element->min_width = 0;
    new_element->min_height = 0;
    new_element->cx = 0;
    new_element->cy = 0;
    new_element->ch = 0;
    new_element->cw = 0;
    new_element->scroll_x = 0;
    new_element->scroll_y = 0;
    new_element->scroll_cumul_x = 0;
    new_element->scroll_cumul_y = 0;
    new_element->cumul_border_values = 0;
    new_element->decal_left = 0;
    new_element->decal_right = 0;
    new_element->decal_top = 0;
    new_element->decal_bottom = 0;

    new_element->type = XGUI_TYPE_DEFAULT;
    new_element->position = XGUI_POSITION_NORMAL;
    new_element->level = 0;
    new_element->font_size = DEFAULT_FONT_SIZE;
    new_element->font_style = XGUI_FONT_STYLE_NORMAL;
    new_element->border_type = XGUI_BORDER_TYPE_NONE;
    new_element->opacity = 255;

    new_element->has_abs_pos_child = false;
    new_element->visibility = true;
    new_element->width_set = false;
    new_element->height_set = false;
    new_element->min_height_set = false;
    new_element->min_width_set = false;
    new_element->max_height_set = false;
    new_element->max_width_set = false;
    new_element->decal_left_set = false;
    new_element->decal_right_set = false;
    new_element->decal_top_set = false;
    new_element->decal_bottom_set = false;
    new_element->texture_emprunted = false;
    new_element->background_texture_emprunted = false;
    new_element->police_emprunted = false;

    return new_element;
}
/* Fin creation STRUCT */





/* Debut mem_free STRUCT */
void XGUI_free_element( XGUI_Element * element_to_free )
{
    if( element_to_free == NULL )
    {
        return;
    }

   if( element_to_free->id != NULL )
    {
        free( element_to_free->id );
        element_to_free->id = NULL;
    }

    if( element_to_free->reference_id != NULL )
    {
        free( element_to_free->reference_id );
        element_to_free->reference_id = NULL;
    }

    if( element_to_free->police_name != NULL )
    {
        free( element_to_free->police_name );
        element_to_free->police_name = NULL;
    }

    if( element_to_free->background_image_source != NULL )
    {
        free( element_to_free->background_image_source );
        element_to_free->background_image_source = NULL;
    }

    if( element_to_free->type_data_source != NULL )
    {
        free( element_to_free->type_data_source );
        element_to_free->type_data_source = NULL;
    }




    if( element_to_free->texture != NULL &&  element_to_free->texture_emprunted == false )
	{
		SDL_DestroyTexture( element_to_free->texture );

        element_to_free->texture = NULL;
	}

	if( element_to_free->background_texture != NULL &&  element_to_free->background_texture_emprunted == false )
	{
		SDL_DestroyTexture( element_to_free->background_texture );

        element_to_free->background_texture = NULL;
	}

	if( element_to_free->police != NULL &&  element_to_free->police_emprunted == false )
	{
		TTF_CloseFont( element_to_free->police );

        element_to_free->police = NULL;
	}

    free( element_to_free );

    element_to_free = NULL;
}
/* Fin mem_free STRUCT */



/* Debut mem_free LIST */
void XGUI_free_element_from( XGUI_Element * chain_first_element )
{
    XGUI_Element *current_element, *next_element;

    if( chain_first_element != NULL )
    {
        current_element = chain_first_element;

        while( current_element != NULL )
        {
            next_element = current_element->next_element;

            XGUI_free_element( current_element );

            current_element = next_element;
        }
    }
}
/* Fin mem_free LIST */

/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, height_setnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_implement_image.h"

int XGUI_implement_image( SDL_Renderer * renderer , XGUI_Element *element , XGUI_Element *root_element , XGUI_Element *root_element_cache )
{
    XGUI_Element *structure_a_verifier = NULL, *prochaine_structure_a_verifier = NULL;

    SDL_Texture *texture_image = NULL;

    int texture_width, texture_height;

    /* RECHERCHE DANS LE CACHE */

    structure_a_verifier = root_element_cache;

    while( texture_image == NULL && structure_a_verifier != NULL && structure_a_verifier != element )
    {
        prochaine_structure_a_verifier = structure_a_verifier->next_element;

        if( strcmp(structure_a_verifier->type_data_source, element->type_data_source) == 0 && structure_a_verifier->type == element->type )
        {
            texture_image = structure_a_verifier->texture;

            element->texture_emprunted = true;
        }

        structure_a_verifier = prochaine_structure_a_verifier;
    }

    /* non trouvé dans le cache => tenter de rechercher dans la chaine */

    if( texture_image == NULL )
    {
        structure_a_verifier = root_element;

        while( texture_image == NULL && structure_a_verifier != NULL && structure_a_verifier != element )
        {
            prochaine_structure_a_verifier = structure_a_verifier->next_element;

            if( strcmp(structure_a_verifier->type_data_source, element->type_data_source) == 0 && structure_a_verifier->type == element->type )
            {
                texture_image = structure_a_verifier->texture;

                element->texture_emprunted = true;
            }

            structure_a_verifier = prochaine_structure_a_verifier;
        }
    }

    /* non trouvé => Créer */

    if( texture_image == NULL )
    {
        texture_image = IMG_LoadTexture( renderer, element->type_data_source );

        if( texture_image == NULL )
        {
            texture_image = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, 200, 200 );

            if( texture_image == NULL )
            {
                errno = ENOMEM;

                return -1;
            }

            SDL_SetRenderDrawColor( renderer, 200, 200, 200, 255 );

            SDL_SetRenderTarget( renderer, texture_image );

            SDL_RenderFillRect( renderer, NULL );

            SDL_SetRenderTarget( renderer, NULL );

        }
    }

    SDL_QueryTexture(texture_image, NULL, NULL, &texture_width, &texture_height);

    if(element->height_set == 0 && element->width_set == 0)
    {
        element->width = texture_width;

        element->height = texture_height;
    }

    if( element->height_set && !element->width_set )
    {
        if( element->decal_top_set && element->decal_bottom_set )
        {
            element->height = element->parent->height - (element->decal_top + element->decal_bottom);
        }

        element->width = element->height * texture_width / texture_height;
    }

    if( element->width_set && !element->height_set )
    {
        if( element->decal_left_set && element->decal_right_set )
        {
            element->width = element->parent->width - (element->decal_left + element->decal_right);
        }

        element->height = element->width * texture_height / texture_width;
    }

    if( element->height_set && element->width_set )
    {
        if(element->decal_top_set && element->decal_bottom_set)
        {
            element->height = element->parent->height - (element->decal_top + element->decal_bottom);
        }

        if(element->decal_left_set && element->decal_right_set)
        {
            element->width = element->parent->width - (element->decal_left + element->decal_right);
        }
    }

    element->texture = texture_image;

    return 0;
}





/* Image de fond */

int XGUI_implement_background_image( SDL_Renderer * renderer , XGUI_Element *element , XGUI_Element *root_element , XGUI_Element *root_element_cache )
{
    XGUI_Element *structure_a_verifier = NULL, *prochaine_structure_a_verifier = NULL;

    SDL_Texture *texture_fond_image = NULL;


    /* RECHERCHE DANS LE CACHE */

    structure_a_verifier = root_element_cache;

    while( texture_fond_image == NULL && structure_a_verifier != NULL && structure_a_verifier != element )
    {
        prochaine_structure_a_verifier = structure_a_verifier->next_element;

        if( strcmp(structure_a_verifier->background_image_source, element->background_image_source) == 0 || ( strcmp(structure_a_verifier->type_data_source, element->type_data_source) == 0 && structure_a_verifier->type == XGUI_TYPE_IMAGE ) )
        {
            if( strcmp(structure_a_verifier->background_image_source, element->type_data_source) == 0 )
            {
                texture_fond_image = structure_a_verifier->texture;
            }
            else
            {
                texture_fond_image = structure_a_verifier->background_texture;
            }

            element->background_texture_emprunted = true;
        }

        structure_a_verifier = prochaine_structure_a_verifier;
    }

    /* non trouvé dans le cache => tenter de rechercher dans la chaine */

    if( texture_fond_image == NULL )
    {
        structure_a_verifier = root_element;

        while( texture_fond_image == NULL && structure_a_verifier != NULL && structure_a_verifier != element )
        {
            prochaine_structure_a_verifier = structure_a_verifier->next_element;

            if( strcmp(structure_a_verifier->background_image_source, element->background_image_source) == 0 || ( strcmp(structure_a_verifier->type_data_source, element->type_data_source) == 0 && structure_a_verifier->type == XGUI_TYPE_IMAGE ) )
            {
                if( strcmp(structure_a_verifier->background_image_source, element->type_data_source) == 0 )
                {
                    texture_fond_image = structure_a_verifier->texture;
                }
                else
                {
                    texture_fond_image = structure_a_verifier->background_texture;
                }

                element->background_texture_emprunted = true;
            }

            structure_a_verifier = prochaine_structure_a_verifier;
        }
    }

    if( texture_fond_image == NULL )
    {
        texture_fond_image = IMG_LoadTexture(renderer, element->background_image_source);

        /* Si le chargement du background échoue => laisser passer */
    }

    element->background_texture = texture_fond_image;

    return 0;
}

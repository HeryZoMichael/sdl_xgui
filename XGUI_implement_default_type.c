/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_implement_default_type.h"

void XGUI_implement_default_type( XGUI_Element *element )
{
    /* UNSPECIFIED TYPE */

    if( element->type == XGUI_TYPE_DEFAULT )
    {
        if( strcmp(element->type_data_source, "__DFL__") != 0 )
        {
            element->type = XGUI_TYPE_TEXT;
        }
        else
        {
            sprintf( element->type_data_source, " ");

            element->type = XGUI_TYPE_BLOC;
        }
    }

    if(element->cursor == DEFAULT)
    {
        if(element->type == XGUI_TYPE_URL)
        {
            element->cursor = HAND;
        }
        else
        {
            XGUI_Element *structure_parent;

            structure_parent = element->parent;

            while( structure_parent != NULL && structure_parent->cursor == DEFAULT )
            {
                element->cursor = structure_parent->cursor;

                structure_parent = structure_parent->parent;
            }
        }
    }

    if( element->width_set == 0 && element->type == XGUI_TYPE_BLOC )
    {
        element->width = element->parent->width;
    }

    if( element->height == 0 && element->type == XGUI_TYPE_BLOC )
    {
        element->height = 1;
    }

    if( element->width == 0 && element->type == XGUI_TYPE_BLOC )
    {
        element->width = 1;
    }

    /* UNSPECIFIED TYPE */
}

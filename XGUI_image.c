/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnsearch_chained_list@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_image.h"

int XGUI_export_as_image( SDL_Window * window , XGUI_Element *search_chained_list , const char *id , int image_format , const char *filename , _Bool override_file )
{
    const char *id_ = (char*) ( (id == NULL) ? search_chained_list->id : id );

    SDL_Renderer * renderer = SDL_GetRenderer(window);

    int ret = 0;

    SDL_Surface *surface_from_renderer = NULL, *surface = NULL;

    SDL_Rect rect;

    int pitch = sizeof(Uint32) * search_chained_list->width;

    Uint32 *tableau_de_pixels = NULL;

    tableau_de_pixels = (Uint32*)(calloc(pitch * search_chained_list->height, sizeof(Uint32)));

    if(tableau_de_pixels == NULL)
    {
        return -1;
    }

    if(SDL_RenderReadPixels(renderer, NULL, SDL_PIXELFORMAT_RGBA8888, tableau_de_pixels, pitch) != 0)
    {
        free( tableau_de_pixels );

        return -1;
    }

    surface_from_renderer = SDL_CreateRGBSurfaceWithFormatFrom(tableau_de_pixels, search_chained_list->width, search_chained_list->height, 32, search_chained_list->width * sizeof(Uint32), SDL_PIXELFORMAT_RGBA8888);

    if(surface_from_renderer == NULL)
    {
        free( tableau_de_pixels );

        return -1;
    }

    rect.x = XGUI_get_attr_value( id_ , _C_X_ , search_chained_list);

    rect.y = XGUI_get_attr_value( id_ , _C_Y_ , search_chained_list);

    rect.w = XGUI_get_attr_value( id_ , _C_W_ , search_chained_list);

    rect.h = XGUI_get_attr_value (id_ , _C_H_ , search_chained_list);

    if( !rect.x && !rect.y && !rect.w && !rect.h)
    {
        return -1;
    }

    surface = SDL_CreateRGBSurface(0, rect.w, rect.h, 32, 0, 0, 0, 0);

    if(surface == NULL)
    {
        free( tableau_de_pixels );

        SDL_FreeSurface(surface_from_renderer);

        return -1;
    }

    if(SDL_BlitSurface(surface_from_renderer, &rect, surface, NULL) != 0)
    {
        free( tableau_de_pixels );

        SDL_FreeSurface(surface_from_renderer);

        SDL_FreeSurface(surface_from_renderer);

        SDL_FreeSurface(surface);

        return -1;
    }

    /* On ne rajoute de pas l'extension */

    if( override_file == false )
    {
        if( access( filename , F_OK ) != -1 )
        {
            /* EXISTS */

            SDL_FreeSurface(surface_from_renderer);

            SDL_FreeSurface(surface);

            free( tableau_de_pixels );

            return -1;
        }
    }

    switch( image_format )
    {
        case XGUI_PNG :
            ret = IMG_SavePNG(surface, filename);
        break;

        default :
            ret = SDL_SaveBMP(surface, filename);
        break;
    }

    SDL_FreeSurface(surface_from_renderer);

    SDL_FreeSurface(surface);

    free( tableau_de_pixels );

    return ret;
}

/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#ifndef DEF_XGUI_CONSTANTS

    #define DEF_XGUI_CONSTANTS

    #define XGUI_MEM_BLOC_LENGTH_MAX	128
    #define XGUI_LARGE_MEM_BLOC_LENGTH_MAX	128*2
    #define XGUI_UINT8_MAX 255
    #define XGUI_INCLUDE_MAX 255


    /* _Bool */
    #define true	1
    #define false	0
    /* _Bool */

    /* Positionnement  des éléments */
    #define XGUI_POSITION_ABSOLUTE		0
    #define XGUI_POSITION_NORMAL		1
    /* Positionnement  des éléments */

    /* Bordures */
    #define XGUI_BORDER_SUPPORT
    #define XGUI_BORDER_TYPE_NONE		0
    #define XGUI_BORDER_TYPE_RADIUS		1
    #define XGUI_BORDER_TYPE_CHANFREIN	2
    /* Bordures */


    /* Style d'écriture des textes */
    #define DEFAULT_FONT_SIZE 						14
    #define XGUI_FONT_STYLE_NORMAL					0
    #define XGUI_FONT_STYLE_BOLD					1
    #define XGUI_FONT_STYLE_ITALIC					2
    #define XGUI_FONT_STYLE_UNDERLINED				3
    #define XGUI_FONT_STYLE_BOLD_ITALIC				4
    #define XGUI_FONT_STYLE_BOLD_UNDERLINED			5
    #define XGUI_FONT_STYLE_ITALIC_UNDERLINED		6
    #define XGUI_FONT_STYLE_BOLD_ITALIC_UNDERLINED	7
    /* Style d'écriture des textes */

    /* Types élements */
    #define XGUI_TYPE_DEFAULT	0
    #define XGUI_TYPE_BLOC		1
    #define XGUI_TYPE_TEXT		2
    #define XGUI_TYPE_IMAGE		3
    #define XGUI_TYPE_PASSWORD	4
    #define XGUI_TYPE_URL		5
    #define XGUI_TYPE_ITEM		6
    #define XGUI_TYPE_ITEM_0	7
    #define XGUI_TYPE_ITEM_1	8
    #define XGUI_TYPE_ITEM_2	9
    #define XGUI_TYPE_ITEM_3	10
    #define XGUI_TYPE_ITEM_4	11
    /* Types élements */


    /* Cur */
    #define XGUI_CUR_DEFAULT	0
    #define XGUI_CUR_LOAD   	1
    #define XGUI_CUR_SEARCH 	2
    #define XGUI_CUR_POINTER	3
    #define XGUI_CUR_ARROW  	4
    #define XGUI_CUR_IBEAM  	5
    /* Cur */


    /* Get value */
    #define __X__   1
    #define __Y__   2
    #define __W__   3
    #define __H__   4
    #define _X_W_   5
    #define _Y_H_   6
    #define X_W_2   7
    #define Y_H_2   8

    #define _C_X_   100
    #define _C_Y_   200
    #define _C_W_   300
    #define _C_H_   400

    #define XGUI_SCROLL_X   1000
    #define XGUI_SCROLL_Y   1001
    /* Get value */

    /* image export */
    #define XGUI_PNG    0
    #define XGUI_BMP    1
    /* image export */

    /* Ordre des feuilles */
    #define XGUI_BASE       0
    #define XGUI_TOP        1
    #define XGUI_SINGLE     2
    #define XGUI_CONTENT    3
    /* Ordre des feuilles */

    #define XGUI_BORDER_SUPPORT

#endif

/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include <stdlib.h>

#include <stdint.h>

#include <SDL2/SDL.h>

#include <SDL2/SDL_ttf.h>

#include "XGUI_constants.h"


#ifndef DEF_XGUI_TYPES

    #define DEF_XGUI_TYPES

    enum ALIGNEMENT
    {
        LEFT, CENTER, RIGHT
    };

    enum CURSOR
    {
        DEFAULT = XGUI_CUR_DEFAULT, LOAD = XGUI_CUR_LOAD, SEARCH = XGUI_CUR_SEARCH, HAND = XGUI_CUR_POINTER, ARROW = XGUI_CUR_ARROW, IBEAM = XGUI_CUR_IBEAM
    };

    typedef struct XGUI_Border_Value_Struct XGUI_Border_Value_Struct;
    struct XGUI_Border_Value_Struct
    {
        uint8_t border_top_left , border_top_right , border_bottom_right , border_bottom_left;
    };

    typedef struct XGUI_Element XGUI_Element;
    struct XGUI_Element
    {
        XGUI_Element *parent, *previous_element, *next_element, *last_not_abs_pos_child;

        XGUI_Border_Value_Struct border_values;

        SDL_Texture *texture, *background_texture;

        TTF_Font *police;

        SDL_Color background_color , text_color;

        enum ALIGNEMENT alignement;
        enum CURSOR cursor;

        char *id , *reference_id , *police_name , *background_image_source , *type_data_source;

        int  x, y, width, height, max_width, max_height, min_width, min_height;
        int cx, cy, ch, cw;
        int scroll_x, scroll_y, scroll_cumul_x, scroll_cumul_y;
        int cumul_border_values;
        int decal_left, decal_right, decal_top, decal_bottom;

        /* Début [0 ; 255] unsigned */
        uint8_t type;
        uint8_t position;
        uint8_t level;
        uint8_t font_size, font_style;
        uint8_t border_type;
        uint8_t opacity;
        /* Fin [0 ; 255] unsigned */

        /* Début boolean */
        _Bool has_abs_pos_child;
        _Bool visibility;
        _Bool width_set , height_set , min_height_set , min_width_set , max_height_set , max_width_set;
        _Bool decal_left_set , decal_right_set , decal_top_set , decal_bottom_set;
        _Bool texture_emprunted , background_texture_emprunted;
        _Bool police_emprunted;
        /* Fin boolean */
    };

#endif

/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#ifndef DEF_XGUI_DISPLAY_LOOP

    #define DEF_XGUI_DISPLAY_LOOP

    #include <stdlib.h>

    #include <SDL2/SDL.h>

    #include <SDL2/SDL_image.h>

    #include <SDL2/SDL_ttf.h>

    #include "XGUI_types.h"

    #include "XGUI_constants.h"

    #include "XGUI_display_worktexture.h"

    #include "XGUI_update_scroll.h"

    int XGUI_display_loop( XGUI_Element *root_element , SDL_Renderer * renderer , SDL_Texture *herited_texture , SDL_Texture *herited_texture_2 , SDL_Texture *herited_texture_4 , SDL_Texture *herited_texture_8 , SDL_Texture *herited_texture_16 );

#endif

/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_display_init_end.h"

int XGUI_display_init( XGUI_Element *root_element , SDL_Renderer * renderer, int page_type , SDL_Texture **herited_texture , SDL_Texture **herited_texture_2 , SDL_Texture **herited_texture_4 , SDL_Texture **herited_texture_8 , SDL_Texture **herited_texture_16 )
{
    XGUI_display_frame_clear( root_element , renderer, page_type );

    return XGUI_display_worktexture_alloc( renderer, root_element, herited_texture , herited_texture_2 , herited_texture_4 , herited_texture_8 , herited_texture_16 );
}

void XGUI_display_close( SDL_Renderer * renderer, int page_type , SDL_Texture *herited_texture , SDL_Texture *herited_texture_2 , SDL_Texture *herited_texture_4 , SDL_Texture *herited_texture_8 , SDL_Texture *herited_texture_16 )
{
    XGUI_display_frame_present( renderer, page_type );

    XGUI_display_worktexture_free( herited_texture , herited_texture_2 , herited_texture_4 , herited_texture_8 , herited_texture_16 );
}

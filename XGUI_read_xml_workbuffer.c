/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_read_xml_workbuffer.h"

int XGUI_Read_workbuffer_alloc( char **attr_buffer, char **balise_text_buffer, char **value_buffer, char **tmp_buffer )
{
    (*attr_buffer) = (*balise_text_buffer) = (*value_buffer) = (*tmp_buffer) = NULL;

    (*attr_buffer) = (char*)malloc( (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) );
    if( (*attr_buffer) == NULL )
    {
        errno = ENOMEM;
        return -1;
    }
    if( (*attr_buffer) != memset( (*attr_buffer) , '\0' , (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) ) )
    {
        errno = ENOMEM;
        free( (*attr_buffer) );
        return -1;
    }

    (*balise_text_buffer) = (char*)malloc( (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) );
    if( (*balise_text_buffer) == NULL )
    {
        errno = ENOMEM;
        free( (*attr_buffer) );
        return -1;
    }

    if( (*balise_text_buffer) != memset( (*balise_text_buffer) , '\0' , (XGUI_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) ) )
    {
        errno = ENOMEM;
        free( (*attr_buffer) );
        free( (*balise_text_buffer) );
        return -1;
    }

    (*value_buffer) = (char*)malloc( ((XGUI_LARGE_MEM_BLOC_LENGTH_MAX)+1)*sizeof(char) );
    if( (*value_buffer) == NULL )
    {
        errno = ENOMEM;
        free( (*attr_buffer) );
        free( (*balise_text_buffer) );
        return -1;
    }
    if( (*value_buffer) != memset( (*value_buffer) , '\0' , ((XGUI_LARGE_MEM_BLOC_LENGTH_MAX)+1)*sizeof(char) ) )
    {
        errno = ENOMEM;
        free( (*attr_buffer) );
        free( (*balise_text_buffer) );
        free( (*value_buffer) );
        return -1;
    }


    (*tmp_buffer) = (char*)malloc( (XGUI_LARGE_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) );
    if( (*tmp_buffer) == NULL )
    {
        errno = ENOMEM;
        free( (*attr_buffer) );
        free( (*balise_text_buffer) );
        free( (*value_buffer) );
        return -1;
    }
    if( (*tmp_buffer) != memset( (*tmp_buffer) , '\0' , (XGUI_LARGE_MEM_BLOC_LENGTH_MAX+1)*sizeof(char) ) )
    {
        errno = ENOMEM;
        free( (*attr_buffer) );
        free( (*balise_text_buffer) );
        free( (*value_buffer) );
        free( (*tmp_buffer) );
        return -1;
    }



    return 0;
}




void XGUI_Read_workbuffer_free( char *attr_buffer, char *balise_text_buffer, char *value_buffer, char *tmp_buffer )
{
    if( attr_buffer != NULL )
    {
        free( attr_buffer );
        attr_buffer = NULL;
    }

    if( balise_text_buffer != NULL )
    {
        free( balise_text_buffer );
        balise_text_buffer = NULL;
    }

    if( value_buffer != NULL )
    {
        free( value_buffer );
        value_buffer = NULL;
    }

    if( tmp_buffer != NULL )
    {
        free( tmp_buffer );
        tmp_buffer = NULL;
    }
}

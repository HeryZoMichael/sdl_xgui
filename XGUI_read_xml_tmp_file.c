/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_read_xml_tmp_file.h"

void XGUI_read_xml_tmp_file_trust( char *filename , char *allocated_new_file_name )
{
    sprintf( allocated_new_file_name, "tmp_file_%ld", (long)getpid() );

    FILE *TMP_FILE_2 = NULL;

    int cur = 0;

    char *projection_fichier = XGUI_mmap( filename );

    if( projection_fichier == NULL )
    {
        return;
    }

    if( (TMP_FILE_2 = fopen(allocated_new_file_name, "w+")) != NULL )
    {
        while( projection_fichier[ cur ] != '\0' )
        {
            if(projection_fichier[ cur ] == '<' || projection_fichier[ cur ] == '>' || projection_fichier[ cur ] == '\\' || projection_fichier[ cur ] == '#' || projection_fichier[ cur ] == '"')
            {
                    fprintf(TMP_FILE_2, "\\%c", projection_fichier[ cur ]);
            }
            else
            {
                if(projection_fichier[ cur ] == '\n')
                {
                    fprintf(TMP_FILE_2, "<b/>");
                }
                else if(projection_fichier[ cur ] == '\t')
                {
                    fprintf(TMP_FILE_2, "<t _=\"    \"/>");
                }
                else if(projection_fichier[ cur ] == ' ')
                {
                    fprintf(TMP_FILE_2, "<t _=\" \"/>");
                }
                else
                {
                    fprintf(TMP_FILE_2, "%c", projection_fichier[ cur ]);
                }
            }

            cur++;

        }

        fclose(TMP_FILE_2);
    }

    XGUI_munmap( projection_fichier );

    return;
}

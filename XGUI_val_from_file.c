/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_val_from_file.h"

void XGUI_val_from_file( char *str )
{
    /* re-vérifier le format */

    if( str != NULL && strlen( str ) >= 5 && str[0] == '{' && str[1] == '{' && str[strlen(str)-2] == '}' && str[strlen(str)-1] == '}' )
    {
        char *template_key = NULL;

        char template_str[XGUI_MEM_BLOC_LENGTH_MAX];

        template_key = (char*)malloc( strlen(str) - 3 ); /* -4+1 = -3 */

        if( template_key == NULL )
        {
            return;
        }
        else
        {

            if( template_key != memset( template_key, '\0', strlen(template_key) ) )
            {
                free(template_key);

                return;
            }
            else
            {

                str[strlen(str)-2] = '\0';  /* enlever les }} cf. "remetre les }}" */

                sprintf( template_key, "%s", (str+2) ); /* enlever les {{ */

                FILE *template_file = NULL;

                if( (template_file = fopen(template_key , "r") ) != NULL) /* ignorer si echec d'ouverture du fichier : existe? */
                {
                    if( fgets(template_str, XGUI_MEM_BLOC_LENGTH_MAX, template_file) != NULL )
                    {
                        if( template_str[strlen(template_str) - 1] == '\n' )
                        {
                                template_str[strlen(template_str) - 1] = '\0';
                        }

                        sprintf(str, "%s", template_str);
                    }
                    else
                    {
                        str[0] = '\0';
                    }

                    fclose(template_file);
                }
                else
                {
                    str[strlen(str)] = '}';  /* remetre les }} cf. "enlever les }}" */
                }

                free(template_key);
            }
        }
    }

    return;

}

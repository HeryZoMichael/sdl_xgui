/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_get_element_from_coord.h"

XGUI_Element * XGUI_get_element_from_coord( SDL_Point * point, XGUI_Element * root_element )
{
    XGUI_Element *structure = NULL, *prochaine_structure = NULL, *last_correspondance = NULL;

    structure = root_element;

    int border_crp = 1;

    SDL_Rect rect_element;

    while(structure != NULL)
    {
        prochaine_structure = structure->next_element;

        rect_element.x = structure->cx;
        rect_element.y = structure->cy;
        rect_element.w = structure->cw;
        rect_element.h = structure->ch;

        if( SDL_PointInRect(point, &rect_element) == SDL_TRUE && structure->opacity != 0 && structure->visibility )
        {
            structure->x += structure->scroll_cumul_x;
            structure->y += structure->scroll_cumul_y;

            #ifdef XGUI_BORDER_SUPPORT

                int border_value_tl, border_value_tr, border_value_br, border_value_bl;

                int abs = 0, ord = 0, y = 0;

                int border_value_cumul = 0;

                SDL_Rect parent;

                XGUI_Element *structure_un_element_parent = NULL;

                border_value_tl = (structure->border_values).border_top_left;
                border_value_tr =  (structure->border_values).border_top_right;
                border_value_br =  (structure->border_values).border_bottom_right;
                border_value_bl =  (structure->border_values).border_bottom_left;

                if( ! ((border_value_tl >= 0 && border_value_tl <= (structure->width)/2 && border_value_tl <= (structure->height)/2) && (border_value_tr >= 0 && border_value_tr <= (structure->width)/2 && border_value_tr <= (structure->height)/2) && (border_value_br >= 0 && border_value_br <= (structure->width)/2 && border_value_br <= (structure->height)/2) && (border_value_bl >= 0 && border_value_bl <= (structure->width)/2 && border_value_bl <= (structure->height)/2)))
                {
                    structure->border_values = (XGUI_Border_Value_Struct){ 0, 0, 0, 0};
                }

                if( border_value_tl != 0 )
                {
                    abs = border_value_tl;

                    while( abs > 0 )
                    {
                        if(structure->border_type == XGUI_BORDER_TYPE_RADIUS)
                        {
                            y = (floor(sqrt(border_value_tl*border_value_tl - abs*abs)) + 0.5 > sqrt(border_value_tl*border_value_tl - abs*abs) ? (int)floor(sqrt(border_value_tl*border_value_tl - abs*abs)) : ceil(sqrt(border_value_tl*border_value_tl - abs*abs)));
                        }
                        else if(structure->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                        {
                            y = border_value_tl - abs;
                        }

                        ord = 0;

                        while(ord <= border_value_tl-y-1)
                        {
                            if( (point->x == structure->x + border_value_tl-abs) && (point->y == structure->y + ord) )
                            {
                                border_crp = 0;
                            }
                            ord++;
                        }
                        abs--;
                    }
                }

                if( border_value_tr != 0 )
                {
                    abs = 0;

                    while(abs <= border_value_tr)
                    {
                        if(structure->border_type == XGUI_BORDER_TYPE_RADIUS)
                        {
                            y = (floor(sqrt(border_value_tr*border_value_tr - abs*abs)) + 0.5 > sqrt(border_value_tr*border_value_tr - abs*abs) ? (int)floor(sqrt(border_value_tr*border_value_tr - abs*abs)) : ceil(sqrt(border_value_tr*border_value_tr - abs*abs)));
                        }
                        else if(structure->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                        {
                            y = border_value_tr - abs;
                        }

                        ord = 0;

                        while(ord <= border_value_tr - y - 1)
                        {
                            if( (point->x == structure->x + (structure->width - border_value_tr + abs - 1)) && (point->y == structure->y + ord) )
                            {
                                border_crp = 0;
                            }
                            ord++;
                        }
                        abs++;
                    }
                }

                if( border_value_bl != 0 )
                {
                    abs = border_value_bl;

                    while(abs > 0)
                    {
                        if(structure->border_type == XGUI_BORDER_TYPE_RADIUS)
                        {
                            y = (floor(sqrt(border_value_bl*border_value_bl - abs*abs)) + 0.5 > sqrt(border_value_bl*border_value_bl - abs*abs) ? (int)floor(sqrt(border_value_bl*border_value_bl - abs*abs)) : ceil(sqrt(border_value_bl*border_value_bl - abs*abs)));
                        }
                        else if(structure->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                        {
                            y = border_value_bl - abs;
                        }

                        ord = structure->height;

                        while(ord >= structure->height - (border_value_bl - y)-1)
                        {
                            if( (point->x == structure->x + border_value_bl-abs) && (point->y == structure->y + ord) )
                            {
                                border_crp = 0;
                            }
                            ord--;
                        }
                        abs--;
                    }
                }

                if( border_value_br != 0 )
                {
                    abs = 0;

                    while(abs < border_value_br)
                    {
                        if(structure->border_type == XGUI_BORDER_TYPE_RADIUS)
                        {
                            y = (floor(sqrt(border_value_br*border_value_br - abs*abs)) + 0.5 > sqrt(border_value_br*border_value_br - abs*abs) ? (int)floor(sqrt(border_value_br*border_value_br - abs*abs)) : ceil(sqrt(border_value_br*border_value_br - abs*abs)));
                        }
                        else if(structure->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                        {
                            y = border_value_br - abs;
                        }

                        ord = structure->height;

                        while(ord >= structure->height - (border_value_br - y)-1)
                        {
                            if( (point->x == structure->x + (structure->width - border_value_br + abs )) && (point->y == structure->y+ ord) )
                            {
                                border_crp = 0;
                            }
                            ord--;
                        }
                        abs++;
                    }
                }

                if( border_crp && structure != root_element )
                {
                    structure_un_element_parent = structure->parent;

                    border_value_cumul = structure->cumul_border_values;

                    while(structure_un_element_parent != NULL && border_value_cumul != 0)
                    {
                        parent.x = structure_un_element_parent->x + structure_un_element_parent->scroll_cumul_x;
                        parent.y = structure_un_element_parent->y + structure_un_element_parent->scroll_cumul_y;
                        parent.w = structure_un_element_parent->width;
                        parent.h = structure_un_element_parent->height;

                        border_value_tl = (structure_un_element_parent->border_values).border_top_left;
                        border_value_tr =  (structure_un_element_parent->border_values).border_top_right;
                        border_value_br =  (structure_un_element_parent->border_values).border_bottom_right;
                        border_value_bl =  (structure_un_element_parent->border_values).border_bottom_left;

                        if( border_value_tl != 0 )
                        {
                            abs = border_value_tl;

                            while(abs >= 0)
                            {
                                if(structure_un_element_parent->border_type == XGUI_BORDER_TYPE_RADIUS)
                                {
                                    y = (floor(sqrt(border_value_tl*border_value_tl - abs*abs)) + 0.5 > sqrt(border_value_tl*border_value_tl - abs*abs) ? (int)floor(sqrt(border_value_tl*border_value_tl - abs*abs)) : ceil(sqrt(border_value_tl*border_value_tl - abs*abs)));
                                }
                                else if(structure_un_element_parent->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y = border_value_tl - abs;
                                }

                                ord = 0;

                                while(ord <= (border_value_tl - y))
                                {
                                    if( (point->x == (parent.x +(border_value_tl - abs) )) && (point->y == (parent.y + ord )) )
                                    {
                                        border_crp = 0;
                                    }
                                    ord++;
                                }
                                abs--;
                            }
                        }

                        if( border_value_tr != 0 )
                        {
                            abs = 0;

                            while(abs <= border_value_tr)
                            {
                                if(structure_un_element_parent->border_type == XGUI_BORDER_TYPE_RADIUS)
                                {
                                    y = (floor(sqrt(border_value_tr*border_value_tr - abs*abs)) + 0.5 > sqrt(border_value_tr*border_value_tr - abs*abs) ? (int)floor(sqrt(border_value_tr*border_value_tr - abs*abs)) : ceil(sqrt(border_value_tr*border_value_tr - abs*abs)));
                                }
                                else if(structure_un_element_parent->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y = border_value_tr - abs;
                                }

                                ord = 0;

                                while(ord <= (border_value_tr - y))
                                {
                                    if( (point->x == (parent.x + parent.w -(border_value_tr - abs) )) && (point->y == (parent.y + ord)) )
                                    {
                                        border_crp = 0;
                                    }
                                    ord++;
                                }
                                abs++;
                            }
                        }

                        if( border_value_bl != 0 )
                        {
                        abs = border_value_bl;

                        while(abs >= 0)
                        {
                            if(structure_un_element_parent->border_type == XGUI_BORDER_TYPE_RADIUS)
                            {
                                y = (floor(sqrt(border_value_bl*border_value_bl - abs*abs)) + 0.5 > sqrt(border_value_bl*border_value_bl - abs*abs) ? (int)floor(sqrt(border_value_bl*border_value_bl - abs*abs)) : ceil(sqrt(border_value_bl*border_value_bl - abs*abs)));
                            }
                            else if(structure_un_element_parent->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                            {
                                y = border_value_bl - abs;
                            }

                            ord = 0;

                            while(ord <= (border_value_bl - y))
                            {
                                if( (point->x == (parent.x + (border_value_bl - abs) )) && (point->y == (parent.y + parent.h - ord)) )
                                {
                                    border_crp = 0;
                                }
                                ord++;
                            }

                            abs--;
                            }
                        }

                        if( border_value_br != 0 )
                        {
                            abs = 0;

                            while(abs <= border_value_br)
                            {
                                if(structure_un_element_parent->border_type == XGUI_BORDER_TYPE_RADIUS)
                                {
                                    y = (floor(sqrt(border_value_br*border_value_br - abs*abs)) + 0.5 > sqrt(border_value_br*border_value_br - abs*abs) ? (int)floor(sqrt(border_value_br*border_value_br - abs*abs)) : ceil(sqrt(border_value_br*border_value_br - abs*abs)));
                                }
                                else if(structure_un_element_parent->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y = border_value_br - abs;
                                }

                                ord = 0;

                                if(	strcmp(SDL_GetPlatform(), "Linux")==0 || strcmp(SDL_GetPlatform(), "Mac OS X")==0	)
                                {
                                    while(ord <= (border_value_br - y))
                                    {
                                        if( (point->x == (parent.x + parent.w - (border_value_br - abs) )) && (point->y == (parent.y + parent.h - ord )) )
                                        {
                                            border_crp = 0;
                                        }
                                        ord++;
                                    }
                                }
                                else
                                {
                                    while(ord <= (border_value_br - y +1))
                                    {
                                        if( (point->x == (parent.x + parent.w - (border_value_br - abs) )) && (point->y == (parent.y + parent.h - ord )) )
                                        {
                                            border_crp = 0;
                                        }
                                        ord++;
                                    }
                                }
                                abs++;
                            }
                        }

                        structure_un_element_parent = structure_un_element_parent->parent;

                        if( !(border_value_tl == 0 && border_value_tr == 0 && border_value_br == 0 && border_value_bl == 0) )
                        {
                            border_value_cumul--;
                        }
                    }
                }
            #endif

            if( border_crp )
            {
                last_correspondance = structure;
            }

            structure->x -= structure->scroll_cumul_x;
            structure->y -= structure->scroll_cumul_y;
        }

        structure = prochaine_structure;

    }

    return last_correspondance;

}

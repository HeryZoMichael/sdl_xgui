/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_display_worktexture.h"

int XGUI_display_worktexture_alloc( SDL_Renderer * renderer, XGUI_Element * root_element, SDL_Texture **herited_texture , SDL_Texture **herited_texture_2 , SDL_Texture **herited_texture_4 , SDL_Texture **herited_texture_8 , SDL_Texture **herited_texture_16 )
{
    (*herited_texture) = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, root_element->width, root_element->height );
    if( (*herited_texture) == NULL )
    {
        errno = ENOMEM;

        return -1;
    }


    (*herited_texture_2) = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, root_element->width/2, root_element->height/2 );
    if( (*herited_texture_2) == NULL )
    {
        errno = ENOMEM;

        SDL_DestroyTexture((*herited_texture));

        return -1;
    }


    (*herited_texture_4) = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, root_element->width/4, root_element->height/4 );
    if( (*herited_texture_4) == NULL )
    {
        errno = ENOMEM;

        SDL_DestroyTexture((*herited_texture));

        SDL_DestroyTexture((*herited_texture_2));

        return -1;
    }


    (*herited_texture_8) = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, root_element->width/8, root_element->height/8 );
    if( (*herited_texture_8) == NULL )
    {
        errno = ENOMEM;

        SDL_DestroyTexture( (*herited_texture) );

        SDL_DestroyTexture( (*herited_texture_2) );

        SDL_DestroyTexture( (*herited_texture_4) );

        return -1;
    }


    (*herited_texture_16) = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, root_element->width/8, root_element->height/16 );
    if( (*herited_texture_16) == NULL )
    {
        errno = ENOMEM;

        SDL_DestroyTexture( (*herited_texture) );

        SDL_DestroyTexture( (*herited_texture_2) );

        SDL_DestroyTexture( (*herited_texture_4) );

        SDL_DestroyTexture( (*herited_texture_8) );

        return -1;
    }

    return 0;
}



void XGUI_display_worktexture_free( SDL_Texture *herited_texture , SDL_Texture *herited_texture_2 , SDL_Texture *herited_texture_4 , SDL_Texture *herited_texture_8 , SDL_Texture *herited_texture_16 )
{
    if( herited_texture != NULL )
    {
        SDL_DestroyTexture( herited_texture );

        herited_texture = NULL;
    }
    if( herited_texture_2 != NULL )
    {
        SDL_DestroyTexture( herited_texture_2 );

        herited_texture_2 = NULL;
    }
    if( herited_texture_4 != NULL )
    {
        SDL_DestroyTexture( herited_texture_4 );

        herited_texture_4 = NULL;
    }
    if( herited_texture_8 != NULL )
    {
        SDL_DestroyTexture( herited_texture_8 );

        herited_texture_8 = NULL;
    }
    if( herited_texture_16 != NULL )
    {
        SDL_DestroyTexture( herited_texture_16 );

        herited_texture_16 = NULL;
    }
}

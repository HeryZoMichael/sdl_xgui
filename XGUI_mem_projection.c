/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_mem_projection.h"

char* XGUI_mmap( const char * filename )
{
    if( filename == NULL )
    {
        errno = EINVAL;

        return NULL;
    }

    int fd = 0;

    char *projection = NULL;

    struct stat FILE_STAT;

    if( stat(filename, &FILE_STAT) == -1 )
    {
        return NULL; /* errno set by stat */
    }

    if( (fd = open(filename, O_RDONLY)) < 0 )
    {
        return NULL; /* errno set by open */
    }

    projection = (char*)mmap(NULL, FILE_STAT.st_size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);

    close(fd);

    if( projection != (char*)MAP_FAILED )
    {
        /* il FAUT ajouter un retour à la ligne dans le fichier, les balises de fin peuvent passer */
        projection[ FILE_STAT.st_size - 1 ] = '\0';

        mprotect(projection, FILE_STAT.st_size, PROT_READ);

        return projection;
    }

    return NULL;
}

int XGUI_munmap( char *projection )
{
    if( projection == NULL ) { errno = EINVAL; return -1; }

    return munmap( (void*)projection, strlen(projection) );
}

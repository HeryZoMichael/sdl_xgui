
/* exemple - 1 */
#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include "SDL_XGUI.h" /* include SDL_XGUI */

#include <sys/time.h>

int main()
{
    	/* SDL-2 Ordinaire */

    	SDL_Window *window = NULL;

    	SDL_Renderer *renderer = NULL;

        SDL_Event event;

    	XGUI_Element *xgui_tree = NULL , *cache = NULL ; /* Pointeu sur la structue pincipale XGUI */



    	SDL_Init(SDL_INIT_VIDEO);

    	TTF_Init();


    	window = SDL_CreateWindow("SDL-2 Ordinaire", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 700, 600, SDL_WINDOW_SHOWN );

		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);


    	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);


    	/* XGUI : Lectue de fichier */

            struct timeval t;

            gettimeofday( &t, NULL);

            int dt = t.tv_usec;

            cache = XGUI_read(window, "RENDU/main.xml", NULL);

            gettimeofday( &t, NULL);

            fprintf(stderr, "Read - 1 - %ld\n", t.tv_usec - dt);

            dt = t.tv_usec;

            xgui_tree = XGUI_read(window, "RENDU/main.xml", cache);

            gettimeofday( &t, NULL);

            fprintf(stderr, "Read - 2 - %ld\n", t.tv_usec - dt);

    	/* XGUI : Affichage */

        dt = t.tv_usec;

    	XGUI_display(window, xgui_tree, XGUI_SINGLE);

        gettimeofday( &t, NULL);

        fprintf(stderr, "Disp - 1 - %ld\n", t.tv_usec - dt);

    	while(1)
    	{
    	        SDL_WaitEvent(&event);

    	        if( event.type == SDL_WINDOWEVENT )
    	        {
    	                if( event.window.event == SDL_WINDOWEVENT_CLOSE )
    	                {
    	                        break;
    	                }
    	        }

    	        SDL_Delay(50);
    	}



    	/* SDL-2 Ordinaire */

    	SDL_DestroyRenderer(renderer);

    	SDL_DestroyWindow(window);


    	/* XGUI : Libération Mémoire */

        XGUI_free_element_from(xgui_tree);
        XGUI_free_element_from(cache);

    	/* SDL-2 Ordinaire */


            TTF_Quit();

            SDL_Quit();

    return 0;
}

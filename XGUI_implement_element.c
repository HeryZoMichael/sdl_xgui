/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnsearch_chained_list@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_implement_element.h"

int XGUI_implement_element(SDL_Renderer * renderer, XGUI_Element * root_element, XGUI_Element * root_element_cache, XGUI_Element * element)
{
    /* BORDURE + VALEUR CUMULÉ */

    element->scroll_cumul_x = element->parent->scroll_cumul_x + element->parent->scroll_x;

    element->scroll_cumul_y = element->parent->scroll_cumul_y + element->parent->scroll_y;

    if( element->cumul_border_values == 0 )
    {
        element->cumul_border_values = element->parent->cumul_border_values;
    }

    /* BORDURE + VALEUR CUMULÉ */


    /* UNSPECIFIED TYPE */

    XGUI_implement_default_type( element );

    /* UNSPECIFIED TYPE */


    /* IMPLEMENTATION TEXT */

    if(element->type == XGUI_TYPE_TEXT || element->type == XGUI_TYPE_PASSWORD || element->type == XGUI_TYPE_URL || element->type == XGUI_TYPE_ITEM_4 || element->type == XGUI_TYPE_ITEM_3 || element->type == XGUI_TYPE_ITEM_2 || element->type == XGUI_TYPE_ITEM_1 || element->type == XGUI_TYPE_ITEM || element->type == XGUI_TYPE_ITEM_0 )
    {
        if( 0 != XGUI_implement_text( renderer , element , root_element , root_element_cache ) )
        {
            return -1;
        }
    }

    /* IMPLEMENTATION TEXT */


    /* IMPLEMENTATION IMAGE */

	if(element->type == XGUI_TYPE_IMAGE)
	{
        if( 0 != XGUI_implement_image( renderer , element , root_element , root_element_cache ) )
        {
            return -1;
        }
	}

	/* IMAGE */


    /* BACKGROUND-IMAGE */

    if( strcmp(element->background_image_source, "__DFL__") != 0 )
    {
        if( 0 != XGUI_implement_background_image( renderer , element , root_element , root_element_cache ) )
        {
            return -1;
        }
    }

    /* BACKGROUND-IMAGE */


    /* POSITIONNEMENT */

    XGUI_implement_position( element );

    /* POSITIONNEMENT */

	return 0;
}

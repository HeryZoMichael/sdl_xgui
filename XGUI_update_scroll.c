/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_update_scroll.h"

void XGUI_update_scroll( XGUI_Element * root_element, const char * id, int attribut, int value )
{
    XGUI_Element *structure = NULL, *prochaine_structure = NULL;

    int level_repere = 0;

    if(value < 0)
    {
        value = 0;
    }

    /*SCROLL-X*/

    if(attribut == XGUI_SCROLL_X)
    {
        structure = root_element;

        while(structure != NULL)
        {
            prochaine_structure = structure->next_element;

            if( strcmp(structure->id, id) == 0 )
            {
                prochaine_structure = NULL;

                level_repere = structure->level;

                if( value >= 0 )
                {
                    structure->scroll_x = value;
                }

                structure = structure->next_element;

                while(structure != NULL && structure->level > level_repere )
                {
                    prochaine_structure = structure->next_element;

                    structure->scroll_cumul_x += (value - structure->scroll_cumul_x);

                    structure = prochaine_structure;
                }
            }

            structure = prochaine_structure;
        }
    }

    /*SCROLL-X*/

    /*SCROLL-Y*/

    if(attribut == XGUI_SCROLL_Y)
    {
        structure = root_element;

        while(structure != NULL)
        {
            prochaine_structure = structure->next_element;

            if( strcmp(structure->id, id) == 0 )
            {
                prochaine_structure = NULL;

                level_repere = structure->level;

                if( value >= 0 )
                {
                    structure->scroll_y = value;
                }

                structure = structure->next_element;

                while(structure != NULL && structure->level > level_repere )
                {
                    prochaine_structure = structure->next_element;

                    structure->scroll_cumul_y += (value - structure->scroll_cumul_y);

                    structure = prochaine_structure;
                }
            }

            structure = prochaine_structure;
        }
    }

    /*SCROLL-Y*/
}

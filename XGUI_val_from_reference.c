/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_val_from_reference.h"

int XGUI_val_from_reference( char *request_code , const char *reference_id , XGUI_Element *search_chained_list )
{
    /* valeur de retour par défaut = 0 */

    int reference_target_value = 0;

    char attr_code[6];

    double i = 0;

    if( strlen(request_code) >= 6 )
    {
        attr_code[0] = request_code[1];
        attr_code[1] = request_code[2];
        attr_code[2] = request_code[3];
        attr_code[3] = request_code[4];
        attr_code[4] = request_code[5];
        attr_code[5] = '\0';

        if( strcmp(attr_code , "__X__") == 0 )
        {
            reference_target_value = XGUI_get_attr_value( reference_id , __X__ , search_chained_list );
        }
        else if( strcmp(attr_code , "__Y__") == 0 )
        {
            reference_target_value = XGUI_get_attr_value( reference_id , __Y__ , search_chained_list );
        }
        else if( strcmp(attr_code , "__W__") == 0 )
        {
            reference_target_value = XGUI_get_attr_value( reference_id , __W__ , search_chained_list );
        }
        else if( strcmp(attr_code , "__H__") == 0 )
        {
            reference_target_value = XGUI_get_attr_value( reference_id , __H__ , search_chained_list );
        }
        else if( strcmp(attr_code , "_X_W_") == 0 )
        {
            reference_target_value = XGUI_get_attr_value( reference_id , _X_W_ , search_chained_list );
        }
        else if( strcmp(attr_code , "_Y_H_") == 0 )
        {
            reference_target_value = XGUI_get_attr_value( reference_id , _Y_H_ , search_chained_list );
        }
        else if( strcmp(attr_code , "X_W_2") == 0 )
        {
            reference_target_value = XGUI_get_attr_value( reference_id , X_W_2 , search_chained_list );
        }
        else if( strcmp(attr_code , "Y_H_2") == 0 )
        {
            reference_target_value = XGUI_get_attr_value( reference_id , Y_H_2 , search_chained_list );
        }

        /* action suplémentaire sur la valeur INT récupérée */

        if( request_code[6] != '\0' )
        {
            if( request_code[6] == '+' || request_code[6] == '-' || request_code[6] == '*' || request_code[6] == '/' )
            {
                 /* izay vao alefa tsirairay */

                request_code[0] = request_code[1] = request_code[2] = request_code[3] = request_code[4] = request_code[5] = '0';

                i = 0;

                if(request_code[6] == '+')
                {
                    request_code[6] = '0';

                    /* Obtenir le "nombre" */

                    if( sscanf(request_code, "%lf", &i) == 1 )
                    {
                        reference_target_value = (int)((double)reference_target_value + i);
                    }
                }
                if(request_code[6] == '-')
                {
                    request_code[6] = '0';

                    if( sscanf(request_code, "%lf", &i) == 1 )
                    {
                        reference_target_value = (int)((double)reference_target_value - i);
                    }
                }
                if(request_code[6] == '*')
                {
                    request_code[6] = '0';

                    if( sscanf(request_code, "%lf", &i) == 1 )
                    {
                        reference_target_value = (int)((double)reference_target_value * i);
                    }
                }
                if(request_code[6] == '/')
                {
                    request_code[6] = '0';

                    if( sscanf(request_code, "%lf", &i) == 1 )
                    {
                        reference_target_value = (int)((double)reference_target_value / i);
                    }
                }
            }
        }
    }

    return reference_target_value;
}

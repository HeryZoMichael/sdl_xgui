/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_implement_text.h"

int XGUI_implement_text( SDL_Renderer * renderer , XGUI_Element *element , XGUI_Element *root_element , XGUI_Element *root_element_cache )
{
    XGUI_Element *structure_a_verifier = NULL;

    SDL_Surface *surface_texte = NULL;

    SDL_Texture *texture_texte = NULL, *texture_item = NULL, *texture_password = NULL;

    TTF_Font *police = NULL;

    int texture_width, texture_height;

    SDL_Rect rect_password;

    size_t password_level = 0;

    int item_level = 0;

    /* RECHERCHE DANS LE CACHE */

    structure_a_verifier = root_element_cache;

    while( texture_texte == NULL && structure_a_verifier != NULL && structure_a_verifier != element )
    {
        if( structure_a_verifier->type == XGUI_TYPE_BLOC || structure_a_verifier->type == XGUI_TYPE_IMAGE )
        {
            structure_a_verifier = structure_a_verifier->next_element;

            continue;
        }

        if( strcmp(structure_a_verifier->police_name, element->police_name) == 0 )
        {
            if( structure_a_verifier->font_size == element->font_size )
            {
                element->police = structure_a_verifier->police;

                element->police_emprunted = true;
            }

            if( strcmp(structure_a_verifier->type_data_source, element->type_data_source) == 0 )
            {
                if( structure_a_verifier->text_color.r == element->text_color.r && structure_a_verifier->text_color.g == element->text_color.g && structure_a_verifier->text_color.b == element->text_color.b && structure_a_verifier->text_color.a == element->text_color.a )
                {
                    if( structure_a_verifier->font_style == element->font_style )
                    {
                        texture_texte = structure_a_verifier->texture;

                        element->texture_emprunted = true;
                    }
                }
            }
        }

        structure_a_verifier = structure_a_verifier->next_element;
    }

    /* non trouvé dans le cache => tenter de rechercher dans la chaine */

    if( texture_texte == NULL )
    {
        structure_a_verifier = root_element;

        while(texture_texte == NULL && structure_a_verifier != NULL && structure_a_verifier != element)
        {
            if( structure_a_verifier->type == XGUI_TYPE_BLOC || structure_a_verifier->type == XGUI_TYPE_IMAGE )
            {
                structure_a_verifier = structure_a_verifier->next_element;

                continue;
            }

            if( strcmp(structure_a_verifier->police_name, element->police_name) == 0 )
            {
                if( structure_a_verifier->font_size == element->font_size )
                {
                    element->police = structure_a_verifier->police;

                    element->police_emprunted = true;
                }

                if( strcmp(structure_a_verifier->type_data_source, element->type_data_source) == 0 )
                {
                    if( structure_a_verifier->text_color.r == element->text_color.r && structure_a_verifier->text_color.g == element->text_color.g && structure_a_verifier->text_color.b == element->text_color.b && structure_a_verifier->text_color.a == element->text_color.a )
                    {
                        if( structure_a_verifier->font_style == element->font_style )
                        {
                            texture_texte = structure_a_verifier->texture;

                            element->texture_emprunted = true;
                        }
                    }
                }
            }

            structure_a_verifier = structure_a_verifier->next_element;
        }
    }

    /* non trouvé => Créer */

    if( texture_texte == NULL )
    {
        if( element->police == NULL )
        {
            element->police = TTF_OpenFont( element->police_name, element->font_size );

            if( element->police == NULL )
            {
                errno = EINVAL;

                return -1;
            }
        }

        police = element->police;

        switch ( element->font_style )
        {
            case XGUI_FONT_STYLE_NORMAL :
                TTF_SetFontStyle(police, TTF_STYLE_NORMAL);
            break;

            case XGUI_FONT_STYLE_BOLD :
                TTF_SetFontStyle(police, TTF_STYLE_BOLD);
            break;

            case XGUI_FONT_STYLE_ITALIC :
                TTF_SetFontStyle(police, TTF_STYLE_ITALIC);
            break;

            case XGUI_FONT_STYLE_UNDERLINED :
                TTF_SetFontStyle(police, TTF_STYLE_UNDERLINE);
            break;

            case XGUI_FONT_STYLE_BOLD_ITALIC :
                TTF_SetFontStyle(police, TTF_STYLE_BOLD | TTF_STYLE_ITALIC);
            break;

            case XGUI_FONT_STYLE_BOLD_UNDERLINED :
                TTF_SetFontStyle(police, TTF_STYLE_BOLD | TTF_STYLE_UNDERLINE);
            break;

            case XGUI_FONT_STYLE_ITALIC_UNDERLINED :
                TTF_SetFontStyle(police, TTF_STYLE_ITALIC | TTF_STYLE_UNDERLINE);
            break;

            case XGUI_FONT_STYLE_BOLD_ITALIC_UNDERLINED :
                TTF_SetFontStyle(police, TTF_STYLE_BOLD | TTF_STYLE_ITALIC | TTF_STYLE_UNDERLINE);
            break;

            default :
                TTF_SetFontStyle(police, TTF_STYLE_NORMAL);
            break;
        }

        if( (element->background_color).a == 255 )
        {
            surface_texte = TTF_RenderUTF8_Shaded( police, element->type_data_source, element->text_color , element->background_color );
        }
        else
        {
            surface_texte = TTF_RenderUTF8_Blended( police, element->type_data_source, element->text_color );
        }

        if( surface_texte == NULL )
        {
            TTF_CloseFont( element->police );

            element->police = NULL;

            errno = ENOMEM;

            return -1;
        }

        texture_texte = SDL_CreateTextureFromSurface( renderer, surface_texte );

        if( texture_texte == NULL )
        {
            TTF_CloseFont( element->police );

            element->police = NULL;

            SDL_FreeSurface(surface_texte);

            errno = ENOMEM;

            return -1;
        }

        SDL_FreeSurface( surface_texte );

        if( element->type == XGUI_TYPE_PASSWORD )
        {
            SDL_QueryTexture( texture_texte, NULL, NULL, &texture_width, &texture_height );

            texture_password = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, strlen(element->type_data_source)*texture_height/2 , texture_height );

            if( texture_password != NULL )
            {
                SDL_SetRenderTarget( renderer, texture_password );

                SDL_SetTextureBlendMode( texture_password, SDL_BLENDMODE_NONE );

                SDL_SetRenderDrawColor( renderer, (element->text_color).r, (element->text_color).g, (element->text_color).b, 0 );

                SDL_RenderFillRect( renderer, NULL );


                if( strcmp(element->type_data_source, " ") != 0 )
                {
                    SDL_SetTextureBlendMode( texture_password, SDL_BLENDMODE_BLEND );

                    SDL_RenderCopy( renderer, texture_password, NULL, NULL );

                    SDL_SetTextureBlendMode( texture_password, SDL_BLENDMODE_NONE );

                }


                while( password_level != strlen(element->type_data_source) )
                {

                    rect_password.x = ((texture_height/4)*(password_level+1)) + (password_level*texture_height/4);

                    rect_password.y = texture_height*3/8;

                    rect_password.w = texture_height/4;

                    rect_password.h = texture_height/4;

                    SDL_SetRenderDrawColor( renderer, (element->text_color).r, (element->text_color).g, (element->text_color).b, (element->text_color).a );

                    SDL_RenderFillRect( renderer, &rect_password );

                    password_level++;
                }

                SDL_SetRenderTarget(renderer, NULL);

                if( element->texture_emprunted == false )
                {
                    SDL_DestroyTexture(texture_texte);
                }

                texture_texte = texture_password;

                texture_password = NULL;
            }
        }

        if( element->type == XGUI_TYPE_ITEM_4 || element->type == XGUI_TYPE_ITEM_3 || element->type == XGUI_TYPE_ITEM_2 || element->type == XGUI_TYPE_ITEM_1 || element->type == XGUI_TYPE_ITEM_0 )
        {
            SDL_QueryTexture(texture_texte, NULL, NULL, &texture_width, &texture_height);

            if(element->type == XGUI_TYPE_ITEM_0){ item_level = 0; }

            if(element->type == XGUI_TYPE_ITEM_1){ item_level = 1; }

            if(element->type == XGUI_TYPE_ITEM_2){ item_level = 2; }

            if(element->type == XGUI_TYPE_ITEM_3){ item_level = 3; }

            if(element->type == XGUI_TYPE_ITEM_4){ item_level = 4; }

            if( strcmp(element->type_data_source, " ") == 0 )
            {
                texture_item = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, (item_level * 30) + (texture_height - (texture_height/3)), texture_height );
            }
            else
            {
                texture_item = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, ((item_level * 30) + texture_width + (texture_height - (texture_height/3)) ), texture_height );
            }

            if( texture_item != NULL )
            {
                SDL_Rect rect_item = {(item_level * 30) + ((texture_height/3)*2), 0, texture_width, texture_height};

                SDL_SetRenderTarget( renderer, texture_item );

                SDL_SetTextureBlendMode( texture_item, SDL_BLENDMODE_NONE );

                SDL_SetRenderDrawColor( renderer, (element->text_color).r, (element->text_color).g, (element->text_color).b, 0 );

                SDL_RenderFillRect( renderer, NULL );

                if( strcmp(element->type_data_source, " ") != 0 )
                {
                    SDL_SetTextureBlendMode( texture_item, SDL_BLENDMODE_BLEND );

                    SDL_RenderCopy( renderer, texture_texte, NULL, &rect_item );

                    SDL_SetTextureBlendMode( texture_item, SDL_BLENDMODE_NONE );

                }

                rect_item.x = item_level * 30;

                rect_item.y = texture_height/3;

                rect_item.w = texture_height/3;

                rect_item.h = texture_height/3;

                SDL_SetRenderDrawColor(renderer, (element->text_color).r, (element->text_color).g, (element->text_color).b, (element->text_color).a );

                SDL_RenderFillRect( renderer, &rect_item );

                SDL_SetRenderTarget( renderer, NULL );

                if( element->texture_emprunted == false )
                {
                    SDL_DestroyTexture( texture_texte );
                }

                texture_texte = texture_item;
            }
        }
    }

    SDL_QueryTexture( texture_texte, NULL, NULL, &texture_width, &texture_height );

    if( !element->height_set )
    {
        element->height = texture_height ;
    }
    if( !element->width_set )
    {
        element->width = texture_width;
    }

    element->texture = texture_texte;

    return 0;

}

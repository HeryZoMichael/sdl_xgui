/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, height_setnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_implement_position.h"

void XGUI_implement_position( XGUI_Element *element )
{
    switch ( element->position )
    {
        case XGUI_POSITION_NORMAL:
            XGUI_implement_position_normal( element );
        break;

        case XGUI_POSITION_ABSOLUTE:
            XGUI_implement_position_absolute( element );
        break;
    }
}

void XGUI_implement_position_normal( XGUI_Element *element )
{

    int total_width_inline = 0, total_placed_inline_center = 0, hauteur_maximale_inline = 0;

	int original_y_de_last = 0;

	XGUI_Element *element_inline = NULL;


    /* normal */

    if( element->type == XGUI_TYPE_BLOC )
    {
        /* Positionnement naturel */

        if(element->parent->last_not_abs_pos_child != NULL)
        {
            element->x = element->parent->x;

            element->y = element->parent->last_not_abs_pos_child->y + element->parent->last_not_abs_pos_child->height;
        }
        else
        {
            element->x = element->parent->x;

            element->y = element->parent->y;
        }

        /* Positionnement naturel */


        /*rectifie f(alignement)*/

        if( element->parent->alignement == LEFT )
        {
        /* ne rien faire */
        }

        /* Pour les bloc on a TOUJOURS un retour à la ligne, même si le bloc est petit */

        if(element->parent->alignement == CENTER)
        {
            element->x += (element->parent->width - element->width)/2;
        }

        if(element->parent->alignement == RIGHT)
        {
            element->x = element->parent->x + element->parent->width - element->width;
        }


        /* XGUI_POSITION_NORMAL => les décalages sont pris en compte un à un */

        element->x -= element->decal_left;

        element->y -= element->decal_top;

        element->x += element->decal_right;

        element->y += element->decal_bottom;
    }

    if( element->type == XGUI_TYPE_TEXT || element->type == XGUI_TYPE_PASSWORD || element->type == XGUI_TYPE_URL || element->type == XGUI_TYPE_ITEM_4 || element->type == XGUI_TYPE_ITEM_3 || element->type == XGUI_TYPE_ITEM_2 || element->type == XGUI_TYPE_ITEM_1 || element->type == XGUI_TYPE_ITEM_0 || element->type == XGUI_TYPE_IMAGE )
    {
        if( element->parent->last_not_abs_pos_child == NULL || element->parent->last_not_abs_pos_child->type == XGUI_TYPE_BLOC )
        {
            /* Le premier sur la ligne */

            if( element->parent->last_not_abs_pos_child == NULL )
            {
                element->x = element->parent->x;

                element->y = element->parent->y;
            }
            else
            {
                element->x = element->parent->x;

                element->y = element->parent->last_not_abs_pos_child->y + element->parent->last_not_abs_pos_child->height;
            }

            /*rectifie f(alignement)*/

            if( element->parent->alignement == LEFT )
            {
            /* ne rien faire */
            }

            if( element->parent->alignement == CENTER )
            {
                element->x += (element->parent->width - element->width)/2;
            }

            if( element->parent->alignement == RIGHT )
            {
                element->x = element->parent->x + element->parent->width - element->width;
            }
        }
        else
        {
            /* Meme parent et sur la meme ligne y+h = y+h du dernier */

            total_width_inline = 0;

            element_inline = element->previous_element;

            while(element_inline != NULL)
            {
                if(
                element_inline->position == XGUI_POSITION_NORMAL
                && ( element_inline->type == XGUI_TYPE_TEXT || element_inline->type == XGUI_TYPE_PASSWORD || element_inline->type == XGUI_TYPE_URL || element_inline->type == XGUI_TYPE_ITEM_4 || element_inline->type == XGUI_TYPE_ITEM_3 || element_inline->type == XGUI_TYPE_ITEM_2 || element_inline->type == XGUI_TYPE_ITEM_1 || element_inline->type == XGUI_TYPE_ITEM_0 || element_inline->type == XGUI_TYPE_IMAGE )
                && ( (element_inline->y + element_inline->height) == (element->previous_element->y + element->previous_element->height) )
                )
                {
                    total_width_inline += element_inline->width;

                    if(element_inline->height > hauteur_maximale_inline)
                    {
                        hauteur_maximale_inline = element_inline->height;
                    }

                    element_inline = element_inline->previous_element;
                }
                else
                {
                    element_inline = NULL;
                }
            }

            /* Si besoin de saut de ligne */

            if( total_width_inline + element->width >= element->parent->width )
            {
                element->x = element->parent->x;

                element->y = element->previous_element->y +element->previous_element->height;

                /* rectifie f(alignement) */

                if( element->parent->alignement == LEFT )
                {
                    /* ne rien faire */
                }

                if( element->parent->alignement == CENTER )
                {
                    element->x += (element->parent->width - element->width)/2;
                }

                if( element->parent->alignement == RIGHT )
                {
                    element->x = element->parent->x + element->parent->width - element->width;
                }

                /*rectifie f(alignement)*/
            }
            else
            {
                /*rectifie f(alignement)*/

                if(element->parent->alignement == LEFT)
                {
                    element->x = element->previous_element->x + element->previous_element->width;
                }

                if(element->parent->alignement == CENTER)
                {
                    total_placed_inline_center = 0;

                    total_width_inline += element->width;

                    element->x = element->parent->x + element->parent->width - (element->parent->width - total_width_inline)/2 - element->width;

                    total_placed_inline_center += element->width;

                    element_inline = element->previous_element;

                    while( element_inline != NULL )
                    {
                        if(
                        (element_inline->type == XGUI_TYPE_TEXT || element_inline->type == XGUI_TYPE_PASSWORD || element_inline->type == XGUI_TYPE_URL || element_inline->type == XGUI_TYPE_ITEM_4 || element_inline->type == XGUI_TYPE_ITEM_3 || element_inline->type == XGUI_TYPE_ITEM_2 || element_inline->type == XGUI_TYPE_ITEM_1 || element_inline->type == XGUI_TYPE_ITEM_0 || element_inline->type == XGUI_TYPE_IMAGE)
                        && element_inline->position == XGUI_POSITION_NORMAL
                        && (element_inline->y + element_inline->height) == (element->previous_element->y + element->previous_element->height)
                        )
                        {
                            element_inline->x = element->parent->x + element->parent->width - (element->parent->width - total_width_inline)/2 - total_placed_inline_center - element_inline->width;

                            total_placed_inline_center += element_inline->width;

                            element_inline = element_inline->previous_element;
                        }
                        else
                        {
                            element_inline = NULL;
                        }
                    }
                }

                if( element->parent->alignement == RIGHT )
                {
                    total_placed_inline_center = 0;

                    total_width_inline += element->width;

                    element->x = element->parent->x + element->parent->width - element->width;

                    total_placed_inline_center += element->width;

                    element_inline = element->previous_element;

                    while(element_inline != NULL)
                    {
                        if(
                        (element_inline->type == XGUI_TYPE_TEXT || element_inline->type == XGUI_TYPE_PASSWORD || element_inline->type == XGUI_TYPE_URL || element_inline->type == XGUI_TYPE_ITEM_4 || element_inline->type == XGUI_TYPE_ITEM_3 || element_inline->type == XGUI_TYPE_ITEM_2 || element_inline->type == XGUI_TYPE_ITEM_1 || element_inline->type == XGUI_TYPE_ITEM_0 || element_inline->type == XGUI_TYPE_IMAGE )
                        && element_inline->position == XGUI_POSITION_NORMAL
                        && (element_inline->y + element_inline->height) == (element->previous_element->y + element->previous_element->height)
                        )
                        {
                            element_inline->x = element->parent->x + element->parent->width - total_placed_inline_center - element_inline->width;

                            total_placed_inline_center += element_inline->width;

                            element_inline = element_inline->previous_element;
                        }
                        else
                        {
                            element_inline = NULL;
                        }
                    }
                }

                /* ENFIN RECTIFIE Y */

                if( hauteur_maximale_inline >= element->height )
                {
                    element->y = element->previous_element->y + element->previous_element->height - element->height;
                }
                else
                {
                    element->y = element->previous_element->y + element->previous_element->height + (element->height - hauteur_maximale_inline) - element->height;

                    original_y_de_last = element->previous_element->y;

                    element_inline = element->previous_element;

                    while(element_inline != NULL)
                    {
                        if(
                        ( element_inline->type == XGUI_TYPE_TEXT || element_inline->type == XGUI_TYPE_PASSWORD || element_inline->type == XGUI_TYPE_URL || element_inline->type == XGUI_TYPE_ITEM_4 || element_inline->type == XGUI_TYPE_ITEM_3 || element_inline->type == XGUI_TYPE_ITEM_2 || element_inline->type == XGUI_TYPE_ITEM_1 || element_inline->type == XGUI_TYPE_ITEM_0 || element_inline->type == XGUI_TYPE_IMAGE )
                        && element_inline->position == XGUI_POSITION_NORMAL
                        && ( (element_inline->y + element_inline->height) == (original_y_de_last + element->previous_element->height) )
                        )
                        {
                            element_inline->y = element->y + element->height -element_inline->height;

                            element_inline = element_inline->previous_element;
                        }
                        else
                        {
                            element_inline = NULL;
                        }
                    }
                }
            }
        }
    }

    /* normal */
}

void XGUI_implement_position_absolute( XGUI_Element *element )
{
    element->x = element->parent->x + element->x;

    element->y = element->parent->y + element->y;

    if( element->decal_left_set && element->decal_right_set )
    {
        element->width = element->parent->width - (element->decal_left + element->decal_right);

        element->x = element->parent->x + element->decal_left;
    }

    if( element->decal_top_set && element->decal_bottom_set )
    {
        element->height = element->parent->height - (element->decal_top + element->decal_bottom);

        element->y = element->parent->y + element->decal_top;
    }

    if( element->decal_top_set && !element->decal_bottom_set )
    {
        element->y = element->parent->y + element->decal_top;
    }

    if( !element->decal_top_set && element->decal_bottom_set )
    {
        element->y = element->parent->y + element->parent->height - element->decal_bottom - element->height;
    }

    if( element->decal_left_set && !element->decal_right_set )
    {
        element->x = element->parent->x + element->decal_left;
    }

    if( !element->decal_left_set && element->decal_right_set )
    {
        element->x = element->parent->x + element->parent->width - element->decal_right - element->width;
    }
}

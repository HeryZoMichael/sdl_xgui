/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_read_xml.h"

XGUI_Element* XGUI_read( SDL_Window * window, const char * filename, XGUI_Element * root_element_cache )
{
    /* THE RENDERER */

    SDL_Renderer * renderer = SDL_GetRenderer( window );

    if( renderer == NULL )
    {
        errno = ENOMEM;

        return NULL;
    }

    /* THE RENDERER */


    /* CREATION ET CONFIGURATION DE LA RACINE */

    XGUI_Element *root_element = XGUI_new_element();

    if( root_element == NULL )
    {
        /* XGUI_new_element() sets ERRNO */

        return NULL;
    }

    sprintf( root_element->id, "ROOT" );

    root_element->type = XGUI_TYPE_BLOC;

    root_element->position = XGUI_POSITION_ABSOLUTE;

    root_element->width_set = true;

    root_element->height_set = true;

    SDL_GetWindowSize(window, &(root_element->width), &(root_element->height));

    root_element->cw = root_element->width;

    root_element->ch = root_element->height;

    /* CREATION ET CONFIGURATION DE LA RACINE */


    /* WORKBUFFER */

    char *attr_buffer = NULL, *balise_text_buffer = NULL, *value_buffer = NULL, *tmp_filename_buffer = NULL;

    if( XGUI_Read_workbuffer_alloc( &attr_buffer , &balise_text_buffer , &value_buffer , &tmp_filename_buffer ) == -1 )
    {
        XGUI_free_element( root_element );

        return NULL;
    }

    /* WORKBUFFER */


    /* PARENT_TAB + PLACER LA RACINE AU SOMMET */

    XGUI_Element *parent = NULL;

    XGUI_Element *tab_parent[XGUI_UINT8_MAX];

    parent = root_element;

    tab_parent[0] = parent;

    /* PARENT_TAB + PLACER LA RACINE AU SOMMET */


    /* RACINE INLINE ET ELEMENT EN COURS DE TRAITEMENT */

    XGUI_Element *root_element_span = NULL , *element_span = NULL , *new_element_span = NULL , *current_element = NULL , *last = NULL;

    root_element_span = XGUI_new_element();

    if( root_element_span == NULL )
    {
        XGUI_free_element( root_element );

        XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

        return NULL;
    }

    element_span = root_element_span;

    current_element = root_element; /* On marque la racine comme élément en cours de traitement */

    /* RACINE INLINE ET ELEMENT EN COURS DE TRAITEMENT */


    /* GESTION DES PROJECTIONS MEMOIRES */

    char *projection_tab[XGUI_INCLUDE_MAX];

    char *projection_active = NULL;

    int projection_cur[XGUI_INCLUDE_MAX];

    int include_level = 0;

    /* GESTION DES PROJECTIONS MEMOIRES */


    int current_caractere = '\0', cur = 0;

    int texte_brute = false, texte_brute_par_slash = false, force_txt_brute_to_zero = false, fast_break = false, block_quit = false, include = false, new_set = true;

    int counter = 0, dsc = 0, last_level = 0;

    int non_trusted_file = false;


    /* CORE PROCEDURE */

    if( (projection_tab[0] = XGUI_mmap(filename)) == NULL )
    {
        XGUI_free_element( root_element );

        XGUI_free_element( root_element_span );

        XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

        return NULL;
    }

    projection_active = projection_tab[0];

    projection_cur[0] = 0;

    while( 1 )
    {
        current_caractere = projection_active[ projection_cur[include_level]++ ];

        if( (current_caractere == '\0') && projection_active == projection_tab[0] ) { break; } /* Fin de fichier racine => Sortie normale */

        /* cur == 0 => Lecture du nom de balise, ou du texte_brute */

        if( cur == 0 )
        {
            start_mem_read:

            texte_brute = true;

            texte_brute_par_slash = fast_break = block_quit = include = false;

            if( force_txt_brute_to_zero )
            {
                texte_brute = false;

                force_txt_brute_to_zero = false;
            }

            /* Lecture du nom de balise ou du texte */

            while( !texte_brute_par_slash && (current_caractere == '\t' || current_caractere == '\n' || current_caractere == ' ' || current_caractere == '<' || current_caractere == '/' || current_caractere == '\r' || current_caractere == '#') && current_caractere != '\0')
            {
                /* LIGNE INSTRUCTION : INCLUDE - SET */

                if( current_caractere == '#' )
                {
                    current_caractere = projection_active[ projection_cur[include_level]++ ];

                    while( current_caractere != '\n' && current_caractere != '\0' )
                    {
                        /* LECTURE DU MOT INSTRUCTION => Arret = Blanc */

                        counter = 0;

                        attr_buffer[0] = '\0';

                        while( current_caractere != '\n' && current_caractere != '\t' && current_caractere != '\r' && current_caractere != ' ' && current_caractere != '\0' )
                        {
                            if( counter < XGUI_MEM_BLOC_LENGTH_MAX )
                            {
                                /* On a alloué XGUI_MEM_BLOC_LENGTH_MAX+1 mais comme ca c'est plus apaisant pour l'esprit :) :D */

                                attr_buffer[counter] = current_caractere;

                                attr_buffer[counter+1] = '\0';

                                counter++;
                            }

                            current_caractere = projection_active[ projection_cur[include_level]++ ];
                        }

                        /* #include */

                        if( strcmp(attr_buffer, "include") == 0 )
                        {
                            while(current_caractere != '\n' && (current_caractere == '\t' || current_caractere == '\r' || current_caractere == ' ' || current_caractere == '"') && current_caractere != '\0')
                            {
                                current_caractere = projection_active[ projection_cur[include_level]++ ];
                            }

                            non_trusted_file = false;

                            counter = 0;

                            value_buffer[0] = '\0';

                            /* lecture du TYPE de fichier */

                            while(current_caractere != '\n' && current_caractere != '\t' && current_caractere != '\r' && current_caractere != ' ' && current_caractere != '"' && current_caractere != '\0')
                            {
                                if(counter < XGUI_MEM_BLOC_LENGTH_MAX && current_caractere != '\0')
                                {
                                    value_buffer[counter] = current_caractere;

                                    value_buffer[counter+1] = '\0';

                                    counter++;
                                }
                                current_caractere = projection_active[ projection_cur[include_level]++ ];
                            }

                            if( strcmp("text/xml", value_buffer) != 0 )
                            {
                                /* NEVER TRUST USER INPUT */

                                non_trusted_file = true;
                            }

                            /* lecture des blancs après le TYPE du fichier */

                            while( current_caractere != '\n' && (current_caractere == '\t' || current_caractere == '\r' || current_caractere == ' ' || current_caractere == '"') && current_caractere != '\0' )
                            {
                                current_caractere = projection_active[ projection_cur[include_level]++ ];
                            }

                            /* lecture du NOM du fichier */

                            counter = 0;

                            value_buffer[0] = '\0';

                            while(current_caractere != '\n' && current_caractere != '\t' && current_caractere != '\r' && current_caractere != ' ' && current_caractere != '"' && current_caractere != '\0')
                            {
                                /* ECHAP */
                                if( current_caractere == '\\')
                                {
                                    current_caractere = projection_active[ projection_cur[include_level]++ ];
                                }

                                if(counter < XGUI_MEM_BLOC_LENGTH_MAX && current_caractere != '\0')
                                {
                                    value_buffer[counter] = (char)current_caractere;

                                    value_buffer[counter+1] = '\0';

                                    counter++;
                                }

                                current_caractere = projection_active[ projection_cur[include_level]++ ];
                            }

                            if( non_trusted_file )
                            {
                                XGUI_read_xml_tmp_file_trust( value_buffer , tmp_filename_buffer );

                                sprintf( value_buffer, "%s", tmp_filename_buffer ); /* Commit */

                                non_trusted_file = false;
                            }

                            while( current_caractere != '\n' && current_caractere != '\0' )
                            {
                                current_caractere = projection_active[ projection_cur[include_level]++ ];
                            }

                            /* Nom de fichier nom vide */

                            if(counter > 0)
                            {
                                if( include_level < XGUI_INCLUDE_MAX )
                                {
                                    projection_tab[include_level + 1] = XGUI_mmap(value_buffer);

                                    if( projection_tab[include_level + 1] != NULL)
                                    {
                                        projection_active = projection_tab[include_level + 1];

                                        projection_cur[include_level + 1] = 0;

                                        include = true;

                                        include_level++;
                                    }
                                }
                            }
                        }
                        else if( strcmp(attr_buffer, "set") == 0 )
                        {
                            /* Paramettrage couleur, font, font-size ... etc ! un peu le span de HTML */

                            new_set = true;

                            loop_for_set : /* Afaka manao set maro */

                            while(current_caractere != '\n' && (current_caractere == '\t' || current_caractere == '\r' || current_caractere == ' ' || current_caractere == '"') && current_caractere != '\0')
                            {
                                current_caractere = projection_active[ projection_cur[include_level]++ ];
                            }

                            /* ny manaraka ny set : back? attr? */

                            counter = 0;

                            attr_buffer[0] = '\0';

                            while(current_caractere != '\n' && current_caractere != '\t' && current_caractere != '\r' && current_caractere != ' ' && current_caractere != '"' && current_caractere != '=' && current_caractere != ':' && current_caractere != '\0')
                            {
                                if( current_caractere == '\\' )
                                {
                                    current_caractere = projection_active[ projection_cur[include_level]++ ];
                                }

                                if(counter < XGUI_MEM_BLOC_LENGTH_MAX && current_caractere != '\0')
                                {
                                    attr_buffer[counter] = current_caractere;

                                    attr_buffer[counter+1] = '\0';

                                    counter++;
                                }

                                current_caractere = projection_active[ projection_cur[include_level]++ ];
                            }

                            /* Faire un rollback vers la dernière valeur => les valeurs du parent? ou de l'avant dernier set */

                            if( strcmp(attr_buffer, "back") == 0 )
                            {
                                if( element_span->previous_element != NULL )
                                {
                                    /* Le premier n'a personne devant lui :p */

                                    element_span = element_span->previous_element;

                                    XGUI_free_element( element_span->next_element );

                                    element_span->next_element = NULL;
                                }
                            }

                            if( counter > 0 && current_caractere != '\0' && !(strcmp(attr_buffer, "back") == 0) )
                            {
                                if(new_set)
                                {
                                    new_element_span = XGUI_new_element();

                                    if( new_element_span == NULL )
                                    {
                                        errno = ENOMEM;

                                        XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                                        while( include_level >= 0 )
                                        {
                                            XGUI_munmap( projection_tab[include_level] );

                                            include_level--;
                                        }

                                        XGUI_free_element_from( root_element );

                                        XGUI_free_element_from( root_element_span );

                                        return NULL;
                                    }

                                    /* pré-remplir le "span" par les valeurs current_element => permet de ne spécifier que certaines valeurs */

                                    new_element_span->font_size = element_span->font_size;

                                    new_element_span->font_style = element_span->font_style;

                                    new_element_span->cursor = element_span->cursor;

                                    new_element_span->opacity = element_span->opacity;

                                    new_element_span->visibility = element_span->visibility;

                                    new_element_span->background_color = element_span->background_color;

                                    new_element_span->text_color = element_span->text_color;

                                    sprintf( new_element_span->police_name, "%s", element_span->police_name );


                                    new_element_span->previous_element = element_span;

                                    element_span->next_element = new_element_span;

                                    element_span = new_element_span;

                                    new_set = false;
                                }

                                while(current_caractere != '\n' && (current_caractere == '\t' || current_caractere == '\r' || current_caractere == ' ' || current_caractere == '"' || current_caractere == '=' || current_caractere == ':') && current_caractere != '\0')
                                {
                                    current_caractere = projection_active[ projection_cur[include_level]++ ];
                                }

                                counter = 0;

                                value_buffer[0] = '\0';

                                while(current_caractere != '\n' && current_caractere != '\t' && current_caractere != '\r' && current_caractere != ' ' && current_caractere != '"' && current_caractere != '\0')
                                {
                                    if( current_caractere == '\\')
                                    {
                                        current_caractere = projection_active[ projection_cur[include_level]++ ];
                                    }

                                    if(counter < XGUI_MEM_BLOC_LENGTH_MAX && current_caractere != '\0')
                                    {
                                        value_buffer[counter] = current_caractere;

                                        value_buffer[counter+1] = '\0';

                                        counter++;
                                    }

                                    current_caractere = projection_active[ projection_cur[include_level]++ ];
                                }

                                if(counter > 0 && current_caractere != '\0')
                                {
                                    XGUI_test_attr_and_write_value( attr_buffer, value_buffer , element_span , tab_parent[0]);

                                    goto loop_for_set; /* permet de faire plusieurs set */
                                }
                            }

                            while(current_caractere != '\n' && current_caractere != '\0')
                            {
                                current_caractere = projection_active[ projection_cur[include_level]++ ];
                            }
                        }
                        else
                        {
                            while(current_caractere != '\n' && current_caractere != '\0')
                            {
                                current_caractere = projection_active[ projection_cur[include_level]++ ];
                            }
                        }
                    }
                }

                /* LIGNE INSTRUCTION : INCLUDE - SET */

                if( include == false )
                {
                    if( current_caractere == '<' )
                    {
                        texte_brute = false;
                    }

                    if( current_caractere == '/' && texte_brute == false )
                    {
                        block_quit = true;
                    }

                    if( current_caractere == '/' && texte_brute )
                    {
                        texte_brute_par_slash = true;
                    }
                    else
                    {
                        current_caractere = projection_active[ projection_cur[include_level]++ ];
                    }
                }
                else
                {
                    current_caractere = '+'; /* un caractère non-Important */
                }
            }

            if( include ) /* remise à 0 des pointeurs */
            {
                cur = 0;

                counter = 0;

                include = false;
            }
            else
            {
                if(current_caractere == '\0')
                {
                    if( projection_active != projection_tab[0])
                    {
                        projection_active = projection_tab[include_level-1];

                        XGUI_munmap(projection_tab[include_level]);

                        include_level--;

                        if(tmp_filename_buffer[0] != '\0')
                        {
                            remove(tmp_filename_buffer);

                            tmp_filename_buffer[0] = '\0';
                        }

                        current_caractere = '\n';

                        goto start_mem_read;
                    }
                    else
                    {
                        goto sortie_douce;
                    }
                }

                counter = 0;

                balise_text_buffer[0] = '\0';

                while(!fast_break && current_caractere != '\t' && current_caractere != '\n' && current_caractere != ' ' && (current_caractere != '>' || (current_caractere == '>' && texte_brute)) && current_caractere != '\r' && current_caractere != '\0' )
                {
                    if(current_caractere == '/' && !texte_brute)
                    {
                        fast_break = 1;
                    }

                    if(current_caractere == '<')
                    {
                        current_caractere = '\n';

                        force_txt_brute_to_zero = 1;
                    }

                    if(current_caractere == '\\')
                    {
                        current_caractere = projection_active[ projection_cur[include_level]++ ];
                    }

                    if(counter < XGUI_MEM_BLOC_LENGTH_MAX && current_caractere != '\0' && current_caractere != '\n' && !fast_break)
                    {
                        balise_text_buffer[counter] = (char)current_caractere;

                        balise_text_buffer[counter+1] = '\0';

                        counter++;
                    }

                    if(counter < XGUI_MEM_BLOC_LENGTH_MAX && current_caractere != '\0' && current_caractere != '\n' && !fast_break)
                    {
                        current_caractere = projection_active[ projection_cur[include_level]++ ];
                    }
                    else
                    {
                        if(texte_brute)
                        {
                            current_caractere = ' ';
                        }
                    }

                    if(counter == XGUI_MEM_BLOC_LENGTH_MAX && current_caractere != '\0' && !fast_break)/*tsy misy dikany ny anaran'ny balise */
                    {
                        while( current_caractere != '\t' && current_caractere != '\n' && current_caractere != ' ' && current_caractere != '\r' && current_caractere != '/' && current_caractere != '>' && current_caractere != '\0' )
                        {
                            current_caractere = projection_active[ projection_cur[include_level]++ ];

                            if(current_caractere == '/')
                            {
                                fast_break = 1;
                            }
                        }
                    }
                }

                if(current_caractere == '\0')
                {
                    if( projection_active != projection_tab[0])
                    {
                        projection_active = projection_tab[include_level-1];

                        XGUI_munmap(projection_tab[include_level]);

                        include_level--;

                        if( tmp_filename_buffer[0] != '\0' )
                        {
                            remove(tmp_filename_buffer);

                            tmp_filename_buffer[0] = '\0';
                        }

                        current_caractere = '\n';

                        goto start_mem_read;
                    }
                    else
                    {
                        goto sortie_douce;
                    }
                }

                if( current_caractere == '>' && texte_brute == 0 && block_quit )
                {
                    if( last_level == dsc )
                    {
                        XGUI_correct_parent( current_element );

                        if( current_element->position == XGUI_POSITION_NORMAL )
                        {
                            current_element->parent->last_not_abs_pos_child = current_element;
                        }
                    }
                    else
                    {
                        XGUI_correct_parent( tab_parent[dsc] );

                        if( tab_parent[dsc]->position == XGUI_POSITION_NORMAL )
                        {
                            tab_parent[dsc]->parent->last_not_abs_pos_child = tab_parent[dsc];
                        }
                    }

                    if( dsc > 0 )
                    {
                        dsc--;
                    }
                    else
                    {
                        XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                        while( include_level >= 0 )
                        {
                            XGUI_munmap( projection_tab[include_level] );

                            include_level--;
                        }

                        XGUI_free_element_from( root_element );

                        XGUI_free_element_from( root_element_span );

                        errno = EINVAL;

                        return NULL;
                    }

                    cur = 0;

                    if( element_span->previous_element != NULL )
                    {
                        element_span = element_span->previous_element;

                        XGUI_free_element( element_span->next_element );

                        element_span->next_element = NULL;
                    }
                }
                else
                {
                    cur = 1;

                    last = current_element;

                    current_element = XGUI_new_element();

                    if( current_element == NULL )
                    {
                        XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                        while( include_level >= 0 )
                        {
                            XGUI_munmap( projection_tab[include_level] );

                            include_level--;
                        }

                        XGUI_free_element_from(root_element);

                        XGUI_free_element_from(root_element_span);

                        return NULL;
                    }

                    /* pré-remplissage du nouvel élément avec les valeurs "current_element" => span */

                    current_element->font_size = element_span->font_size;

                    current_element->font_style = element_span->font_style;

                    current_element->cursor = element_span->cursor;

                    current_element->opacity = element_span->opacity;

                    current_element->visibility = element_span->visibility;

                    current_element->text_color = element_span->text_color;

                    current_element->background_color = element_span->background_color;

                    sprintf(current_element->police_name, "%s", element_span->police_name);

                    if( dsc < XGUI_UINT8_MAX )
                    {
                        dsc++;
                    }
                    else
                    {
                        XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                        while( include_level >= 0 )
                        {
                            XGUI_munmap( projection_tab[include_level] );

                            include_level--;
                        }

                        XGUI_free_element(current_element);

                        XGUI_free_element_from(root_element);

                        XGUI_free_element_from(root_element_span);

                        errno = EINVAL;

                        return NULL;
                    }

                    if( dsc == last_level )
                    {
                        current_element->parent = tab_parent[dsc-1];

                        current_element->previous_element = last;
                    }
                    else
                    {
                        if( dsc > last_level )
                        {
                            current_element->parent = last;

                            current_element->previous_element = NULL;

                            tab_parent[dsc-1] = current_element->parent;
                        }
                        else
                        {
                            current_element->parent = tab_parent[dsc-1];

                            current_element->previous_element = tab_parent[dsc];
                        }

                        parent = tab_parent[dsc-1];
                    }

                    last->next_element = current_element;

                    current_element->next_element = NULL;

                    last_level = dsc;

                    if( texte_brute )
                    {
                        sprintf( current_element->id, "%s", current_element->parent->id );

                        current_element->type = XGUI_TYPE_TEXT;

                        if( strlen(balise_text_buffer) < XGUI_MEM_BLOC_LENGTH_MAX )
                        {
                            sprintf( current_element->type_data_source, "%s ", balise_text_buffer );
                        }
                        else
                        {
                            sprintf( current_element->type_data_source, "%s", balise_text_buffer );
                        }

                        current_element->level = dsc;

                        if( XGUI_implement_element(renderer, root_element, root_element_cache, current_element) != 0 )
                        {
                            XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                            while( include_level >= 0 )
                            {
                                XGUI_munmap( projection_tab[include_level] );

                                include_level--;
                            }

                            XGUI_free_element_from(root_element);

                            XGUI_free_element_from(root_element_span);

                            return NULL;
                        }

                        XGUI_correct_parent(current_element);

                        if( dsc > 0 )
                        {
                            dsc--;
                        }
                        else
                        {
                            XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                            while( include_level >= 0 )
                            {
                                XGUI_munmap( projection_tab[include_level] );

                                include_level--;
                            }

                            XGUI_free_element_from(root_element);

                            XGUI_free_element_from(root_element_span);

                            errno = ENOMEM;

                            return NULL;
                        }

                        cur = 0;

                        if( current_element->position == XGUI_POSITION_NORMAL )
                        {
                            current_element->parent->last_not_abs_pos_child = current_element;
                        }
                    }
                }
            }
        }

        if( cur == 1 )
        {
            counter = 0;

            while((current_caractere == '\t' || current_caractere == '\n' || current_caractere == ' ' || current_caractere == '\r') && current_caractere != '\0')
            {
                current_caractere = projection_active[ projection_cur[include_level]++ ];
            }

            if(current_caractere == '\0')
            {
                if( projection_active != projection_tab[0])
                {
                    projection_active = projection_tab[include_level-1];

                    XGUI_munmap( projection_tab[include_level] );

                    include_level--;

                    if(tmp_filename_buffer[0] != '\0')
                    {
                        remove(tmp_filename_buffer);

                        tmp_filename_buffer[0] = '\0';
                    }

                    current_caractere = '\n';

                    goto start_mem_read;
                }
                else
                {
                    goto sortie_douce;
                }
            }

            if( current_caractere == '>' || current_caractere == '/' )
            {
                if( current_caractere == '>' || fast_break )
                {
                    new_element_span = XGUI_new_element();

                    if( new_element_span == NULL )
                    {
                        XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                        while( include_level >= 0 )
                        {
                            XGUI_munmap( projection_tab[include_level] );

                            include_level--;
                        }

                        XGUI_free_element_from(root_element);

                        XGUI_free_element_from(root_element_span);

                        return NULL;
                    }

                    new_element_span->font_size = element_span->font_size;

                    new_element_span->font_style = element_span->font_style;

                    new_element_span->cursor = element_span->cursor;

                    new_element_span->opacity = element_span->opacity;

                    new_element_span->visibility = element_span->visibility;

                    sprintf(new_element_span->police_name, "%s", element_span->police_name);

                    new_element_span->background_color = element_span->background_color;

                    new_element_span->text_color = element_span->text_color;


                    new_element_span->previous_element = element_span;

                    element_span->next_element = new_element_span;

                    element_span = new_element_span;


                    element_span->font_size = current_element->font_size;

                    element_span->font_style = current_element->font_style;

                    element_span->cursor = current_element->cursor;

                    element_span->opacity = current_element->opacity;

                    element_span->visibility = current_element->visibility;

                    sprintf(element_span->police_name, "%s", current_element->police_name);

                    element_span->background_color = current_element->background_color;

                    element_span->text_color = current_element->text_color;

                    current_element->level = dsc;


                    if( XGUI_implement_element(renderer, root_element, root_element_cache, current_element) != 0 )
                    {
                        XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                        while( include_level >= 0 )
                        {
                            XGUI_munmap( projection_tab[include_level] );

                            include_level--;
                        }

                        XGUI_free_element_from(root_element);

                        XGUI_free_element_from(root_element_span);

                        return NULL;
                    }

                    cur = 0;
                }

                if( current_caractere == '/' || fast_break )
                {
                    if( !fast_break )
                    {
                        current_element->level = dsc;

                        if(XGUI_implement_element(renderer, root_element, root_element_cache, current_element) != 0)
                        {
                            XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                            while( include_level >= 0 )
                            {
                                XGUI_munmap( projection_tab[include_level] );

                                include_level--;
                            }

                            XGUI_free_element_from(root_element);

                            XGUI_free_element_from(root_element_span);

                            errno = ENOMEM;

                            return NULL;
                        }
                    }
                    else
                    {
                        if( element_span->previous_element != NULL )
                        {
                            element_span = element_span->previous_element;

                            XGUI_free_element( element_span->next_element );

                            element_span->next_element = NULL;
                        }
                    }

                    XGUI_correct_parent( current_element );

                    if( dsc > 0 )
                    {
                        dsc--;
                    }
                    else
                    {
                        XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

                        while( include_level >= 0 )
                        {
                            XGUI_munmap( projection_tab[include_level] );

                            include_level--;
                        }

                        XGUI_free_element_from(root_element);

                        XGUI_free_element_from(root_element_span);

                        errno = ENOMEM;

                        return NULL;
                    }

                    cur = 0;

                    if(current_element->position == XGUI_POSITION_NORMAL)
                    {
                        current_element->parent->last_not_abs_pos_child = current_element;
                    }

                    while((current_caractere = projection_active[ projection_cur[include_level]++ ]) != '>')
                    {
                    //rien
                    }
                }
            }
            else
            {
                while(current_caractere != '\t' && current_caractere != '\n' && current_caractere != ' ' && current_caractere != '\r' && current_caractere != '=' && current_caractere != '\0')
                {
                    if( counter < XGUI_MEM_BLOC_LENGTH_MAX )
                    {
                        attr_buffer[counter] = current_caractere;

                        attr_buffer[counter+1] = '\0';

                        counter++;
                    }

                    current_caractere = projection_active[ projection_cur[include_level]++ ];
                }

                if(current_caractere == '\0')
                {
                    if( projection_active != projection_tab[0])
                    {
                        projection_active = projection_tab[include_level-1];

                        XGUI_munmap(projection_tab[include_level]);

                        include_level--;

                        if(tmp_filename_buffer[0] != '\0')
                        {
                            remove(tmp_filename_buffer);tmp_filename_buffer[0] = '\0';
                        }

                        current_caractere = '\n';

                        goto start_mem_read;
                    }
                    else
                    {
                        goto sortie_douce;
                    }
                }

                cur = 2;
            }
        }

        if(cur == 2)
        {
            counter = 0;

            while(current_caractere != '"' && current_caractere != '\0')
            {
                current_caractere = projection_active[ projection_cur[include_level]++ ];
            }

            if(current_caractere == '\0')
            {
                if( projection_active != projection_tab[0])
                {
                    projection_active = projection_tab[include_level-1];

                    XGUI_munmap(projection_tab[include_level]);

                    include_level--;

                    if(tmp_filename_buffer[0] != '\0')
                    {
                        remove(tmp_filename_buffer);

                        tmp_filename_buffer[0] = '\0';
                    }

                    current_caractere = '\n';

                    goto start_mem_read;
                }
                else
                {
                    goto sortie_douce;
                }
            }

            current_caractere = projection_active[ projection_cur[include_level]++ ];

            if( current_caractere == '"' )
            {
                value_buffer[0] = ' ';

                value_buffer[1] = '\0';
            }
            else
            {
                counter = 0;

                value_buffer[0] = '\0';

                while( current_caractere != '"' && current_caractere != '\0' )
                {
                    if(current_caractere == '\\')
                    {
                        current_caractere = projection_active[ projection_cur[include_level]++ ];

                        if(current_caractere == '\0')
                        {
                            if( projection_active != projection_tab[0])
                            {
                                projection_active = projection_tab[include_level-1];

                                XGUI_munmap(projection_tab[include_level]);

                                include_level--;

                                if(tmp_filename_buffer[0] != '\0')
                                {
                                    remove(tmp_filename_buffer);

                                    tmp_filename_buffer[0] = '\0';
                                }

                                current_caractere = '\n';

                                goto start_mem_read;
                            }
                            else
                            {
                                goto sortie_douce;
                            }
                        }
                    }

                    if(counter < XGUI_MEM_BLOC_LENGTH_MAX)
                    {
                        value_buffer[counter] = current_caractere;

                        value_buffer[counter+1] = '\0';

                        counter++;
                    }
                    else
                    {
                        if( strcmp(attr_buffer, "value") == 0
                        || strcmp(attr_buffer, "src") == 0
                        || strcmp(attr_buffer, "_v") == 0
                        || strcmp(attr_buffer, "background-image") == 0 )
                        {
                            if( counter < XGUI_LARGE_MEM_BLOC_LENGTH_MAX )
                            {
                                value_buffer[counter] = current_caractere;

                                value_buffer[counter+1] = '\0';

                                counter++;
                            }
                        }
                    }
                    current_caractere = projection_active[ projection_cur[include_level]++ ];
                }
            }

            if(current_caractere == '\0')
            {
                if( projection_active != projection_tab[0])
                {
                    projection_active = projection_tab[include_level-1];

                    XGUI_munmap(projection_tab[include_level]);

                    include_level--;if(tmp_filename_buffer[0] != '\0')
                    {
                        remove(tmp_filename_buffer);

                        tmp_filename_buffer[0] = '\0';
                    }

                    current_caractere = '\n';

                    goto start_mem_read;
                }
                else
                {
                    goto sortie_douce;
                }
            }

            cur = 1;

            XGUI_test_attr_and_write_value( attr_buffer, value_buffer , current_element , tab_parent[0]);

        }
    }

    sortie_douce :

    XGUI_munmap( projection_tab[0] );

    XGUI_Read_workbuffer_free( attr_buffer , balise_text_buffer , value_buffer , tmp_filename_buffer );

    XGUI_free_element_from( root_element_span );

    return root_element;

    /* CORE PROCEDURE */
}

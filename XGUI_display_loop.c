/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_display_loop.h"

int XGUI_display_loop( XGUI_Element *root_element , SDL_Renderer * renderer, SDL_Texture *herited_texture , SDL_Texture *herited_texture_2 , SDL_Texture *herited_texture_4 , SDL_Texture *herited_texture_8 , SDL_Texture *herited_texture_16 )
{
    SDL_Rect actuel, parent, intersection;

    SDL_Rect src_rect, dst_rect, rect_background;

    XGUI_Element *structure_a_afficher = NULL, *prochaine_structure_a_afficher = NULL;

    SDL_Texture *texture_actuel_complet = NULL;

    SDL_Texture *current_texture = NULL;

    int largeur_image_fond, hauteur_image_fond, texture_fond_fin_y, texture_fond_fin_x;

    int new_texture=0;




    structure_a_afficher = root_element->next_element;

    while( structure_a_afficher != NULL )
    {
        prochaine_structure_a_afficher = structure_a_afficher->next_element;


        actuel.x = structure_a_afficher->x-structure_a_afficher->scroll_cumul_x;

        actuel.y = structure_a_afficher->y-structure_a_afficher->scroll_cumul_y;

        actuel.w = structure_a_afficher->width;

        actuel.h = structure_a_afficher->height;

        parent.x = structure_a_afficher->parent->cx;

        parent.y = structure_a_afficher->parent->cy;

        parent.w = structure_a_afficher->parent->cw;

        parent.h = structure_a_afficher->parent->ch;


        if( SDL_IntersectRect(&actuel, &parent, &intersection) == SDL_FALSE || structure_a_afficher->visibility != true )
        {
            if( SDL_IntersectRect(&actuel, &parent, &intersection) == SDL_FALSE )
            {
                if( !structure_a_afficher->parent->has_abs_pos_child )
                {
                    if( structure_a_afficher != structure_a_afficher->parent->last_not_abs_pos_child )
                    {
                        if( structure_a_afficher->scroll_cumul_x == 0 && structure_a_afficher->scroll_cumul_y == 0 )
                        {
                            if( structure_a_afficher->parent->cw >= structure_a_afficher->parent->width )
                            {
                                prochaine_structure_a_afficher = structure_a_afficher->parent->last_not_abs_pos_child;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if( structure_a_afficher->width <= root_element->width/8 && structure_a_afficher->height <= root_element->height/16 )
            {
                texture_actuel_complet = herited_texture_16;
            }
            else if( structure_a_afficher->width <= root_element->width/8 && structure_a_afficher->height <= root_element->height/8 )
            {
                texture_actuel_complet = herited_texture_8;
            }
            else if( structure_a_afficher->width <= root_element->width/4 && structure_a_afficher->height <= root_element->height/4 )
            {
                texture_actuel_complet = herited_texture_4;
            }
            else if( structure_a_afficher->width <= root_element->width/2 && structure_a_afficher->height <= root_element->height/2 )
            {
                texture_actuel_complet = herited_texture_2;
            }
            else if( structure_a_afficher->width <= root_element->width && structure_a_afficher->height <= root_element->height )
            {
                texture_actuel_complet = herited_texture;
            }
            else
            {
                new_texture = 1;

                if( (texture_actuel_complet = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, actuel.w, actuel.h) ) == NULL )
                {
                    XGUI_display_worktexture_free( herited_texture , herited_texture_2 , herited_texture_4 , herited_texture_8 , herited_texture_16 );

                    errno = ENOMEM;

                    return -1;
                }
            }

            SDL_SetRenderTarget( renderer, texture_actuel_complet );

            SDL_RenderClear( renderer );

            SDL_SetRenderTarget( renderer, NULL );

            rect_background.x = 0;

            rect_background.y = 0;

            rect_background.w = actuel.w;

            rect_background.h = actuel.h;

            SDL_SetRenderTarget(renderer, texture_actuel_complet);

            SDL_SetTextureBlendMode(texture_actuel_complet, SDL_BLENDMODE_NONE);




            SDL_Rect rect_texture_fond, rect_texture_fond_copie;

            /* SKIP TEXTE AVEC FOND UNIS CF IMPLEMENT TEXT SHADED */

            if( !((structure_a_afficher->background_color).a == 255 && (structure_a_afficher->type == XGUI_TYPE_TEXT || structure_a_afficher->type == XGUI_TYPE_PASSWORD || structure_a_afficher->type == XGUI_TYPE_URL || structure_a_afficher->type == XGUI_TYPE_ITEM_4 || structure_a_afficher->type == XGUI_TYPE_ITEM_3 || structure_a_afficher->type == XGUI_TYPE_ITEM_2 || structure_a_afficher->type == XGUI_TYPE_ITEM_1 || structure_a_afficher->type == XGUI_TYPE_ITEM_0 )) )
            {
                /* BYPASS IMAGE SANS TRANSPARENCE */

                if( !(	structure_a_afficher->type == XGUI_TYPE_IMAGE
                    && (
                    strcasecmp( structure_a_afficher->type_data_source+(strlen(structure_a_afficher->type_data_source)-3), "bmp" ) == 0
                    ||
                    strcasecmp( structure_a_afficher->type_data_source+(strlen(structure_a_afficher->type_data_source)-3), "jpg" ) == 0
                    ||
                    strcasecmp( structure_a_afficher->type_data_source+(strlen(structure_a_afficher->type_data_source)-3), "gif" ) == 0
                    ||
                    strcasecmp( structure_a_afficher->type_data_source+(strlen(structure_a_afficher->type_data_source)-4), "jpeg" ) == 0
                    )
                ))
                {
                    SDL_SetRenderDrawColor( renderer, (structure_a_afficher->background_color).r, (structure_a_afficher->background_color).g, (structure_a_afficher->background_color).b, (structure_a_afficher->background_color).a );

                    SDL_RenderFillRect( renderer, &rect_background );
                }
            }


            if( structure_a_afficher->background_texture != NULL )
            {
                SDL_QueryTexture( structure_a_afficher->background_texture, NULL, NULL, &largeur_image_fond, &hauteur_image_fond );

                rect_texture_fond.x = 0;

                rect_texture_fond.y = 0;

                rect_texture_fond.w = largeur_image_fond;

                rect_texture_fond.h = hauteur_image_fond;

                texture_fond_fin_y = 0;

                while( texture_fond_fin_y == 0 )
                {
                    if( (rect_texture_fond.y + hauteur_image_fond) <= (rect_background.y + rect_background.h) )
                    {
                        rect_texture_fond.h = hauteur_image_fond;
                    }
                    else
                    {
                        texture_fond_fin_y = 1;

                        rect_texture_fond.h = rect_background.h - rect_texture_fond.y;
                    }

                    texture_fond_fin_x = 0;

                    rect_texture_fond.x = 0;

                    while( texture_fond_fin_x == 0 )
                    {
                        if( (rect_texture_fond.x + largeur_image_fond) <= (rect_background.x + rect_background.w) )
                        {
                            rect_texture_fond.w = largeur_image_fond;
                        }
                        else
                        {
                            texture_fond_fin_x = 1;

                            rect_texture_fond.w = rect_background.w - rect_texture_fond.x;
                        }

                        rect_texture_fond_copie.x = 0;

                        rect_texture_fond_copie.y = 0;

                        rect_texture_fond_copie.w = rect_texture_fond.w;

                        rect_texture_fond_copie.h = rect_texture_fond.h;

                        SDL_RenderCopy( renderer, structure_a_afficher->background_texture, &rect_texture_fond_copie, &rect_texture_fond );

                        rect_texture_fond.x += largeur_image_fond;
                    }

                    rect_texture_fond.y += hauteur_image_fond;
                }
            }




            SDL_SetTextureBlendMode( current_texture, SDL_BLENDMODE_BLEND );

            SDL_SetRenderTarget( renderer, texture_actuel_complet );

            current_texture = structure_a_afficher->texture;

            SDL_RenderCopy( renderer, current_texture, NULL, &rect_background );


            /* XGUI_BORDER_SUPPORT */

            #ifdef XGUI_BORDER_SUPPORT

                SDL_Point point;

                int border_value_cumul = 0;

                XGUI_Element *structure_un_element_parent = NULL;

                int border_value_tl, border_value_tr, border_value_br, border_value_bl;

                int abs=0, ord=0, y=0;

                if( structure_a_afficher->cumul_border_values - structure_a_afficher->parent->cumul_border_values == 1 )
                {
                    border_value_tl = (structure_a_afficher->border_values).border_top_left;
                    border_value_tr =  (structure_a_afficher->border_values).border_top_right;
                    border_value_br =  (structure_a_afficher->border_values).border_bottom_right;
                    border_value_bl =  (structure_a_afficher->border_values).border_bottom_left;

                    if( ! ((border_value_tl >= 0 && border_value_tl <= (structure_a_afficher->width)/2 && border_value_tl <= (structure_a_afficher->height)/2) && (border_value_tr >= 0 && border_value_tr <= (structure_a_afficher->width)/2 && border_value_tr <= (structure_a_afficher->height)/2) && (border_value_br >= 0 && border_value_br <= (structure_a_afficher->width)/2 && border_value_br <= (structure_a_afficher->height)/2) && (border_value_bl >= 0 && border_value_bl <= (structure_a_afficher->width)/2 && border_value_bl <= (structure_a_afficher->height)/2)))
                    {
                        structure_a_afficher->border_values = (XGUI_Border_Value_Struct){ 0, 0, 0, 0};
                    }

                    SDL_SetRenderTarget(renderer, texture_actuel_complet);

                    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);

                    if( (intersection.x-actuel.x) <= border_value_tl && (intersection.y-actuel.y) <= border_value_tl )
                    {
                        if( border_value_tl != 0 )
                        {
                            abs = border_value_tl;

                            while( abs > 0 )
                            {
                                if(structure_a_afficher->border_type == XGUI_BORDER_TYPE_RADIUS)
                                {
                                    y = (floor(sqrt(border_value_tl*border_value_tl-abs*abs)) + 0.5 > sqrt(border_value_tl*border_value_tl-abs*abs) ? (int)floor(sqrt(border_value_tl*border_value_tl-abs*abs)) : ceil(sqrt(border_value_tl*border_value_tl-abs*abs)));
                                }
                                else if(structure_a_afficher->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y = border_value_tl-abs;
                                }

                                SDL_RenderDrawLine(renderer, border_value_tl-abs, 0, border_value_tl-abs, border_value_tl-y-1);

                                abs--;
                            }
                        }
                    }

                    if( ((actuel.x + actuel.w)-(intersection.x + intersection.w)) <= border_value_tr && (intersection.y-actuel.y) <= border_value_tr )
                    {
                    if( border_value_tr != 0 )
                    {
                        abs = 0;

                        while( abs <= border_value_tr )
                        {
                            if( structure_a_afficher->border_type == XGUI_BORDER_TYPE_RADIUS )
                            {
                                y = (floor(sqrt(border_value_tr*border_value_tr-abs*abs)) + 0.5 > sqrt(border_value_tr*border_value_tr-abs*abs) ? (int)floor(sqrt(border_value_tr*border_value_tr-abs*abs)) : ceil(sqrt(border_value_tr*border_value_tr-abs*abs)));
                            }
                            else if( structure_a_afficher->border_type == XGUI_BORDER_TYPE_CHANFREIN )
                            {
                                y = border_value_tr-abs;
                            }

                            SDL_RenderDrawLine(renderer, actuel.w-border_value_tr+abs-1, 0, actuel.w-border_value_tr+abs-1, border_value_tr-y-1);

                            abs++;
                        }
                        }
                    }

                    if( (intersection.x-actuel.x) <= border_value_bl && ((actuel.y + actuel.h)-(intersection.y + intersection.h)) <= border_value_bl )
                    {
                        if( border_value_bl != 0 )
                        {
                            abs = border_value_bl;

                            while( abs > 0 )
                            {
                                if( structure_a_afficher->border_type == XGUI_BORDER_TYPE_RADIUS )
                                {
                                    y = (floor(sqrt(border_value_bl*border_value_bl-abs*abs)) + 0.5 > sqrt(border_value_bl*border_value_bl-abs*abs) ? (int)floor(sqrt(border_value_bl*border_value_bl-abs*abs)) : ceil(sqrt(border_value_bl*border_value_bl-abs*abs)));
                                }
                                else if(structure_a_afficher->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y = border_value_bl-abs;
                                }

                                SDL_RenderDrawLine( renderer, border_value_bl-abs, actuel.h-1, border_value_bl-abs, actuel.h-(border_value_bl-y)-1 );

                                abs--;
                            }
                        }
                    }

                    if(((actuel.x + actuel.w)-(intersection.x + intersection.w)) <= border_value_br && ((actuel.y + actuel.h)-(intersection.y + intersection.h)) <= border_value_br)
                    {
                        if(border_value_br != 0)
                        {
                            abs = 0;

                            while(abs < border_value_br)
                            {
                                if(structure_a_afficher->border_type == XGUI_BORDER_TYPE_RADIUS)
                                {
                                    y=(floor(sqrt(border_value_br*border_value_br-abs*abs)) + 0.5 > sqrt(border_value_br*border_value_br-abs*abs) ? (int)floor(sqrt(border_value_br*border_value_br-abs*abs)) : ceil(sqrt(border_value_br*border_value_br-abs*abs)));
                                }
                                else if(structure_a_afficher->border_type == XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y=border_value_br-abs;
                                }

                                SDL_RenderDrawLine(renderer, actuel.w-border_value_br + abs, actuel.h , actuel.w-border_value_br + abs,  actuel.h-(border_value_br-y)-1);

                                abs++;
                            }
                        }
                    }
                }

                structure_un_element_parent=structure_a_afficher->parent;

                border_value_cumul=structure_a_afficher->cumul_border_values;

                while(structure_un_element_parent != NULL && border_value_cumul != 0 )
                {
                    parent.x = structure_un_element_parent->x + structure_un_element_parent->scroll_cumul_x;
                    parent.y = structure_un_element_parent->y + structure_un_element_parent->scroll_cumul_y;
                    parent.w = structure_un_element_parent->width;
                    parent.h = structure_un_element_parent->height;

                    border_value_tl = (structure_un_element_parent->border_values).border_top_left;
                    border_value_tr =  (structure_un_element_parent->border_values).border_top_right;
                    border_value_br =  (structure_un_element_parent->border_values).border_bottom_right;
                    border_value_bl =  (structure_un_element_parent->border_values).border_bottom_left;


                    SDL_SetRenderTarget(renderer, texture_actuel_complet);

                    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);

                    if((intersection.x-structure_un_element_parent->x) <= border_value_tl && (intersection.y-structure_un_element_parent->y)<=border_value_tl)
                    {
                        if(border_value_tl != 0)
                        {
                            abs=border_value_tl;

                            while(abs > 0)
                            {
                                if(structure_un_element_parent->border_type==XGUI_BORDER_TYPE_RADIUS)
                                {
                                    y = (floor(sqrt(border_value_tl*border_value_tl-abs*abs)) + 0.5 > sqrt(border_value_tl*border_value_tl-abs*abs) ? (int)floor(sqrt(border_value_tl*border_value_tl-abs*abs)) : ceil(sqrt(border_value_tl*border_value_tl-abs*abs)));
                                }
                                else if(structure_un_element_parent->border_type==XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y = border_value_tl-abs;
                                }

                                ord=0;

                                while(ord <= (border_value_tl-y-1) )
                                {
                                    point.x = parent.x + (border_value_tl-abs);

                                    point.y = parent.y + ord;

                                    if(SDL_PointInRect(&point, &actuel) == SDL_TRUE )
                                    {
                                        SDL_RenderDrawPoint(renderer, point.x-actuel.x, point.y-actuel.y);
                                    }
                                    ord++;
                                }
                                abs--;
                            }
                        }
                    }

                    if( ((structure_un_element_parent->x + structure_un_element_parent->width)-(intersection.x + intersection.w)) <= border_value_tr && (intersection.y-structure_un_element_parent->y) <= border_value_tr )
                    {
                        if( border_value_tr != 0 )
                        {
                            abs = 0;

                            while( abs < border_value_tr+1 )
                            {
                                if(structure_un_element_parent->border_type==XGUI_BORDER_TYPE_RADIUS)
                                {
                                    y = (floor(sqrt(border_value_tr*border_value_tr-abs*abs)) + 0.5 > sqrt(border_value_tr*border_value_tr-abs*abs) ? (int)floor(sqrt(border_value_tr*border_value_tr-abs*abs)) : ceil(sqrt(border_value_tr*border_value_tr-abs*abs)));
                                }
                                else if(structure_un_element_parent->border_type==XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y = border_value_tr-abs;
                                }

                                ord=0;

                                while( ord <= (border_value_tr-y-1) )
                                {
                                    point.x = parent.x + parent.w-border_value_tr+abs-1;

                                    point.y = parent.y + ord;

                                    if( SDL_PointInRect(&point, &actuel) == SDL_TRUE )
                                    {
                                        SDL_RenderDrawPoint(renderer, point.x-actuel.x, point.y-actuel.y);
                                    }
                                    ord++;
                                }
                                abs++;
                            }
                        }
                    }

                    if((intersection.x-structure_un_element_parent->x) <= border_value_bl && ((structure_un_element_parent->y + structure_un_element_parent->height)-(intersection.y + intersection.h)) <= border_value_bl)
                    {
                        if(border_value_bl != 0)
                        {
                            abs=border_value_bl;

                            while(abs >= 0)
                            {
                                if(structure_un_element_parent->border_type==XGUI_BORDER_TYPE_RADIUS)
                                {
                                    y=(floor(sqrt(border_value_bl*border_value_bl-abs*abs)) + 0.5 > sqrt(border_value_bl*border_value_bl-abs*abs) ? (int)floor(sqrt(border_value_bl*border_value_bl-abs*abs)) : ceil(sqrt(border_value_bl*border_value_bl-abs*abs)));
                                }
                                else if(structure_un_element_parent->border_type==XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y=border_value_bl-abs;
                                }

                                ord=0;

                                while(ord <= (border_value_bl-y))
                                {
                                    point.x=parent.x + (border_value_bl-abs);

                                    point.y=parent.y + parent.h-ord;

                                    if(SDL_PointInRect(&point, &actuel) == SDL_TRUE)
                                    {
                                        SDL_RenderDrawPoint(renderer, point.x-actuel.x, point.y-actuel.y);
                                    }
                                    ord++;
                                }
                                abs--;
                            }
                        }
                    }

                    if( ((structure_un_element_parent->x + structure_un_element_parent->width)-(intersection.x + intersection.w)) <= border_value_br && ((structure_un_element_parent->y + structure_un_element_parent->height)-(intersection.y + intersection.h)) <= border_value_br )
                    {
                        if(border_value_br != 0)
                        {
                            abs=0;

                            while(abs <= border_value_br)
                            {
                                if(structure_un_element_parent->border_type==XGUI_BORDER_TYPE_RADIUS)
                                {
                                    y=(floor(sqrt(border_value_br*border_value_br-abs*abs)) + 0.5 > sqrt(border_value_br*border_value_br-abs*abs) ? (int)floor(sqrt(border_value_br*border_value_br-abs*abs)) : ceil(sqrt(border_value_br*border_value_br-abs*abs)));
                                }
                                else if(structure_un_element_parent->border_type==XGUI_BORDER_TYPE_CHANFREIN)
                                {
                                    y=border_value_br-abs;
                                }

                                ord=0;

                                if(	strcmp(SDL_GetPlatform(), "Linux")==0 || strcmp(SDL_GetPlatform(), "Mac OS X")==0	)
                                {
                                    while(ord <= border_value_br-y)
                                    {
                                        point.x=parent.x + parent.w-(border_value_br-abs) ;

                                        point.y=parent.y + parent.h-ord;

                                        if(SDL_PointInRect(&point, &actuel) == SDL_TRUE)
                                        {
                                            SDL_RenderDrawPoint(renderer, point.x-actuel.x, point.y-actuel.y);
                                        }
                                        ord++;
                                    }
                                }
                                else
                                {
                                    while(ord <= (border_value_br-y + 1))
                                    {
                                        point.x=parent.x + parent.w-(border_value_br-abs) ;

                                        point.y=parent.y + parent.h-ord;

                                        if(SDL_PointInRect(&point, &actuel) == SDL_TRUE)
                                        {
                                            SDL_RenderDrawPoint(renderer, point.x-actuel.x, point.y-actuel.y);
                                        }
                                        ord++;
                                    }
                                }
                                abs++;
                            }
                        }
                    }

                    if(!(border_value_tl == 0 && border_value_tr == 0 && border_value_br == 0 && border_value_bl == 0))
                    {
                        border_value_cumul--;
                    }

                    structure_un_element_parent=structure_un_element_parent->parent;

                }

            #endif

            /* XGUI_BORDER_SUPPORT */





            SDL_SetTextureBlendMode( texture_actuel_complet, SDL_BLENDMODE_BLEND );

            SDL_SetRenderTarget( renderer, NULL );

            src_rect.x = intersection.x - actuel.x;

            src_rect.y = intersection.y - actuel.y;

            src_rect.w = intersection.w;

            src_rect.h = intersection.h;

            dst_rect.x = intersection.x;

            dst_rect.y = intersection.y;

            dst_rect.w = intersection.w;

            dst_rect.h = intersection.h;

            if( structure_a_afficher->type != XGUI_TYPE_BLOC )
            {
                SDL_SetTextureAlphaMod( texture_actuel_complet, structure_a_afficher->opacity );
            }

            SDL_RenderCopy( renderer, texture_actuel_complet, &src_rect, &dst_rect );

            if( new_texture )
            {
                SDL_DestroyTexture(texture_actuel_complet);

                new_texture = 0;
            }

            /* mise a jour des dimensions visible de la structure_a_afficher */

            structure_a_afficher->cx = intersection.x;

            structure_a_afficher->cy = intersection.y;

            structure_a_afficher->cw = intersection.w;

            structure_a_afficher->ch = intersection.h;

        }

        structure_a_afficher = prochaine_structure_a_afficher;

    }

    return 0;
}

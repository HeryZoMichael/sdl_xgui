/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnsearch_chained_list@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#ifndef DEF_XGUI_IMPLEMENT_ELEMENT

    #define DEF_XGUI_IMPLEMENT_ELEMENT

    #include <stdio.h>

    #include <unistd.h>

    #include <stdlib.h>

    #include <string.h>

    #include <math.h>

    #include <errno.h>


    #include "XGUI_types.h"

    #include "XGUI_constants.h"

    #include "XGUI_implement_default_type.h"

    #include "XGUI_implement_image.h"

    #include "XGUI_implement_text.h"

    #include "XGUI_implement_position.h"


    #include <SDL2/SDL.h>

    #include <SDL2/SDL_image.h>

    #include <SDL2/SDL_ttf.h>

    int XGUI_implement_element(SDL_Renderer * renderer, XGUI_Element * root_element, XGUI_Element * root_element_cache, XGUI_Element * element);

#endif

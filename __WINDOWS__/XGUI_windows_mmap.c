/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "__WINDOWS__/XGUI_windows_mmap.h"

void * mmap( void *start, size_t length, int prot, int flags, int fd, off_t offset )
{
    HANDLE hmap;

    void *temp;

    size_t len;

    struct stat st;

    uint64_t o = offset;

    uint32_t l = o & 0xFFFFFFFF;

    uint32_t h = (o >> 32) & 0xFFFFFFFF;

    if( !fstat(fd, &st) )
    {
        len = (size_t) st.st_size;
    }
    else
    {
        return MAP_FAILED;
    }

    if( (length + offset) > len )
    {
        length = len - offset;
    }

    if( !(flags & MAP_PRIVATE) )
    {
        return MAP_FAILED;
    }

    hmap = CreateFileMapping((HANDLE)_get_osfhandle(fd), 0, PAGE_WRITECOPY, 0, 0, 0);

    if( !hmap )
    {
        return MAP_FAILED;
    }

    temp = MapViewOfFileEx(hmap, FILE_MAP_COPY, h, l, length, start);

    CloseHandle(hmap);

    return temp ? temp : MAP_FAILED;

}


int munmap(void *start, size_t length)
{
    return !UnmapViewOfFile(start);
}

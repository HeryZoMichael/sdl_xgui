/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_color.h"

SDL_Color XGUI_get_color_from_code( char *color_code )
{
    SDL_Color color = { 0 , 0 , 0 , 0 };

	double double_r, double_g, double_b, double_a;

    if( color_code == NULL )
    {
        return color;
    }

    if( color_code[0] == '#' )
    {
        char i_[10] ="#FFFFFFFF"; /* "#RRGGBBAA"; */

        const char *list = "0123456789ABCDEF";

        char *s_, *s__;

        size_t len_list = strlen(list);

        size_t len_i = 9;

        uint8_t tab_rgba[4];

        size_t i = 0;

        if( strlen(color_code) == 4 ) /* #RGB */
        {
            i_[0] = color_code[0];
            i_[1] = i_[2] = color_code[1];
            i_[3] = i_[4] = color_code[2];
            i_[5] = i_[6] = color_code[3];
            i_[7] = i_[8] = 'F';
            i_[9] = '\0';
        }
        else if( strlen(color_code) == 5 ) /* #RGBA*/
        {
            i_[0] = color_code[0];
            i_[1] = i_[2] = color_code[1];
            i_[3] = i_[4] = color_code[2];
            i_[5] = i_[6] = color_code[3];
            i_[7] = i_[8] = color_code[4];
            i_[9] = '\0';
        }
        else if( strlen(color_code) == 7 ) /* #RRGGBB */
        {
            sprintf( i_, "%sFF", color_code );
        }
        else if( strlen(color_code) == 9 ) /* #RRGGBBAA */
        {
            sprintf( i_, "%s", color_code );
        }

        while( i < len_i-1 )
        {
            if( (s_ = (char *)memchr((void *)list, toupper((i_+1)[i]), len_list)) != NULL && (s__ = (char *)memchr((void *)list, toupper((i_+1)[i+1]), len_list)) != NULL )
            {
                i+=2;

                tab_rgba[(i/2)-1] = (uint8_t)((len_list - strlen( s__ )) + ((len_list - strlen( s_ )) * 16));
            }
            else{ tab_rgba[0] = tab_rgba[1] = tab_rgba[2] = 255; break; }
        }

        color = (SDL_Color){ tab_rgba[0] , tab_rgba[1] , tab_rgba[2] , tab_rgba[3] };
    }
    else
    {
        if( sscanf(color_code, "%lf %lf %lf %lf", &double_r, &double_g, &double_b, &double_a) == 3 )
        {
            color = (SDL_Color){ (uint8_t)double_r , (uint8_t)double_g , (uint8_t)double_b , 255 };
        }
        else if( sscanf(color_code, "%lf %lf %lf %lf", &double_r, &double_g, &double_b, &double_a) == 4 )
        {
            color = (SDL_Color){ (uint8_t)double_r , (uint8_t)double_g , (uint8_t)double_b , (uint8_t)double_a };
        }
    }

    return color;
}

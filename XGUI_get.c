/*

Copyright (C) 2019 Written with SDL-2

RABENANDRASANA Hery Zo Michael, hsnroot@gmail.com toornsh@gmail.com, +261345986940 +261329699874

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1.	The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2.	Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3.	This notice may not be removed or altered from any source distribution.

*/

#include "XGUI_get.h"


XGUI_Element * XGUI_get_element_by_id( const char *id , XGUI_Element *search_chained_list )
{
    XGUI_Element *current_element;

    current_element = search_chained_list;

    while( current_element != NULL )
    {
        if( strcmp(current_element->id, id) == 0 )
        {
            return current_element;
        }
        current_element = current_element->next_element;
    }

    return NULL;
}



int XGUI_get_attr_value( const char *id , const int request , XGUI_Element *search_chained_list )
{
    XGUI_Element *element;

    element = XGUI_get_element_by_id( id , search_chained_list );

    if( element )
    {
        switch( request )
        {
            case __X__:
                return element->x;
            break;

            case __Y__:
                return element->y;
            break;

            case __W__:
                return element->width;
            break;

            case __H__:
                return element->height;
            break;

            case _X_W_:
                return element->x + element->width;
            break;

            case _Y_H_:
                return element->y + element->height;
            break;

            case X_W_2:
                return element->x + (element->width/2);
            break;

            case Y_H_2:
                return element->y + (element->height/2);
            break;

            case _C_X_:
                return element->cx;
            break;

            case _C_Y_:
                return element->cy;
            break;

            case _C_W_:
                return element->cw;
            break;

            case _C_H_:
                return element->ch;
            break;

            case XGUI_SCROLL_X:
                return element->scroll_x;
            break;

            case XGUI_SCROLL_Y:
                return element->scroll_y;
            break;

            default:
                return -1;
            break;
        }
    }

    return -1;
}
